FROM harbor.maxiv.lu.se/dockerhub/library/node:15.8.0-alpine3.10
WORKDIR /home/app
COPY . .
RUN npm install --no-cache
CMD ["npm", "start"]
EXPOSE 4000