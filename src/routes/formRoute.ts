import express from "express";
import { Model } from "mongoose";
import { getFormConfig, getFormConfigs } from "../config";
import * as counters from "../database/counters";
import { FormDocument } from "../models/mongoSchema";
import {
  addSystemEvent,
  getEmptyFormTypeObject,
  getFormAccess,
  getLatestSupervisorAction,
  getOldestSupervisorAction,
} from "../utilities/Helpers";
import * as mailer from "../utilities/mailer";

export const router = express.Router();

router.put("/file", async function (req, res) {
  try {
    const { id, formType, fileName, size } = req.body;
    const formConfig: System.FormConfig = getFormConfig(
      formType as System.FormType
    );
    const form = await formConfig.model.findById(id);
    form.files.push({ name: fileName, size });
    await form.save();
    const formUpdate: System.FormUpdate = {
      form,
      updateType: "ATTACHMENT",
    };
    if (formConfig.onEdit) {
      if (formConfig.onEdit(formUpdate, req.access)) {
        mailer.editNotification(form, req.access);
      }
    } else {
      mailer.editNotification(form, req.access);
    }
    const formAccess = await getFormAccess(form.formType, form._id, req.access);
    addSystemEvent(
      formUpdate.form,
      {
        action: "Attachment",
        agent: req.access.name,
        role: formAccess.isSubmitter ? "Submitter" : "Administrator",
        description: `Uploaded additional file attachment for this submission (${fileName})`,
        color: "rgb(222, 147, 0)",
      },
      1000
    );
    res.status(200).send();
  } catch (err) {
    return res.status(500).json(err);
  }
});

router.post("/clone", async function (req, res) {
  const { access } = req;

  const formType = req.body.formType as System.FormType;
  const id = req.body.id as string;
  const formConfig = getFormConfig(formType);
  try {
    const formAccess = await getFormAccess(formType, id, access);
    if (
      formAccess.readAccess !== "DEFAULT" &&
      formAccess.readAccess !== "FULL"
    ) {
      return res.status(403).send();
    }
    if (!formAccess.form) {
      return res.status(404).send();
    }
    const forms = await formConfig.model.find({
      _id: id,
    });
    const form = forms[0].toObject() as System.Form;
    const oldestAction = getOldestSupervisorAction(form);
    const oldId = form._id;
    form._id = await counters.getNext(formType);
    form.status = "DRAFT";
    form.externalId = "";
    form.edits = [];
    form.comments = [
      {
        eventType: "Comment",
        timestamp: new Date(),
        updated: new Date(),
        source: "CLONE COMMENT",
        msg: `This submission is a clone of ${formConfig.name} #${oldId}.`,
        author: "Domni",
        status: form.status,
        supervisor: null,
        role: "System",
      },
    ];
    form.supervisorActions = [
      {
        eventType: "SupervisorAction",
        name: oldestAction.name,
        email: oldestAction.email,
        action: "PENDING",
        timestamp: new Date(),
        decisionDate: undefined,
        comment: "",
        notify: false,
        role: oldestAction.role,
      },
    ];
    form.systemEvents = [
      {
        eventType: "SystemEvent",
        timestamp: new Date(Date.now() + 1000),
        action: "Cloned",
        agent: `${access.name}`,
        role: "Administrator",
        description: `Cloning of the pre-existing form ${id}`,
        color: "black",
      },
    ];

    const result = await formConfig.model.create(form);
    if (formConfig.onSubmit) {
      if (formConfig.onSubmit(result)) {
        mailer.submissionNotification(result);
        mailer.submissionConfirmation(result);
      }
    } else {
      mailer.submissionNotification(result);
      mailer.submissionConfirmation(result);
    }
    return res.status(200).send(result);
  } catch (error) {
    console.log("Error", error);
    res.status(500).send();
  }
});
// Create
router.post("/", async function (req, res) {
  const { access } = req;
  const form = req.body as System.Form;
  const { formType } = form;
  const formConfig = getFormConfig(formType);
  try {
    form._id = await counters.getNext(formType);
    form.submitterName = access.name;
    form.submitterEmail = access.email;
    form.submissionDate = form.status === "PENDING" ? new Date() : undefined;
    const result = (await formConfig.model.create(form)) as System.Form;
    console.log(
      `Created ${form.formType} ${result._id} for ${result.submitterName} (${form.status})`
    );
    if (form.status === "DRAFT") {
      return res.status(200).send(result);
    }
    if (formConfig.onSubmit) {
      if (formConfig.onSubmit(result)) {
        mailer.submissionNotification(result);
        mailer.submissionConfirmation(result);
      }
    } else {
      mailer.submissionNotification(result);
      mailer.submissionConfirmation(result);
    }
    return res.status(200).send(result);
  } catch (err) {
    console.log("POST FAILED", err);
    return res.status(500).json(err);
  }
});

// For form approval, rejection or reassignment. This endpoint assumes that any nessessary supervisorAction, comments or edit already is added to the form
router.put("/process/", async (req, res) => {
  const { access } = req;
  const formUpdate = req.body as System.FormUpdate;
  const { form, updateType, disableNotify, comment } = formUpdate;
  const { formType } = form;
  const formConfig = getFormConfig(formType);
  try {
    const formAccess = await getFormAccess(form.formType, form._id, access);
    if (
      !formAccess.isAdmin &&
      !formAccess.isSupervisor &&
      !formAccess.canEdit
    ) {
      return res.status(403).send();
    }
    const updatedForm: System.Form = await formConfig.model.findByIdAndUpdate(
      form._id,
      form,
      {
        new: true,
      }
    );
    console.log(
      `Updated ${form.formType} ${updatedForm._id} for ${updatedForm.submitterName}`
    );
    if (updateType === "SUPERVISOR_REASSIGNMENT") {
      if (formConfig.onReassign) {
        if (formConfig.onReassign(formUpdate, access) && !disableNotify) {
          mailer.reassignmentNotification(updatedForm, access, comment);
        }
      } else if (!disableNotify) {
        mailer.reassignmentNotification(updatedForm, access, comment);
      }
      // approval or reject
    } else {
      if (formConfig.onDecision) {
        if (formConfig.onDecision(formUpdate, access)) {
          mailer.decisionNotificationSubmitter({
            form: updatedForm,
            decider: access,
            requiresApproval: formConfig.requiresApproval,
            comment,
          });
          if (!disableNotify) {
            mailer.decisionNotificationListeners(updatedForm, access);
          }
        }
      } else {
        mailer.decisionNotificationSubmitter({
          form: updatedForm,
          decider: access,
          requiresApproval: formConfig.requiresApproval,
          comment,
        });
        if (!disableNotify) {
          mailer.decisionNotificationListeners(updatedForm, access);
        }
      }
    }
    res.status(200).send(updatedForm);
  } catch (error) {
    res.status(error.code).send();
  }
});
/**
 *  Update of a form that isn't a decision (approve, reject or reassign). I.e., draft submittal, resubmittal, regular edit, comment, external id, archiving, reminder postponing.
 *  In case of RESUBMITTAL or EDIT formUpdate is expected to be of type FormUpdateWithEdit.
 *  In case of COMMENT formUpdate is expected to be of type FormUpdateWithComment
 *  comments, edits and supervisorActions in form are ignored because they may be missing due to limited read access, and are always re-read from the db.
 */
router.put("/", async (req, res) => {
  const { access } = req;
  const formUpdate = req.body as System.FormUpdate;
  const { form, updateType, disableNotify } = formUpdate;
  const { formType } = form;
  const formConfig = getFormConfig(formType);
  try {
    const formAccess = await getFormAccess(form.formType, form._id, access);
    if (
      !formAccess.isAdmin &&
      !formAccess.isSupervisor &&
      !formAccess.canEdit
    ) {
      return res.status(403).send();
    }
    const oldForm = await formConfig.model.findById(form._id);
    const { edits, comments } = oldForm;
    const newComments = [...comments];
    if (formUpdate.comment) {
      newComments.push(formUpdate.comment);
    }
    const newEdits = [...edits];
    if (formUpdate.edit) {
      newEdits.push(formUpdate.edit);
    }
    if (updateType === "DRAFT_SUBMITTAL") {
      form.submissionDate = new Date();
      form.status = "PENDING";
    }
    if (updateType === "RESUBMITTAL") {
      form.status = "PENDING"; // update form REJECTED back to pending if resubmitting
    }
    form.edits = newEdits;
    form.comments = newComments;
    let updatedForm: System.Form = await formConfig.model.findByIdAndUpdate(
      form._id,
      form,
      {
        new: true,
      }
    );
    console.log(
      `Updated ${form.formType} ${updatedForm._id} for ${updatedForm.submitterName}`
    );

    // Handle creation of actions, comments and edits, as well as executing post-action hooks. A bit messy with giant switch
    switch (updateType) {
      case "DRAFT_SUBMITTAL":
        if (formConfig.onSubmit) {
          if (formConfig.onSubmit(updatedForm)) {
            mailer.submissionNotification(updatedForm);
            mailer.submissionConfirmation(updatedForm);
          }
        } else {
          mailer.submissionNotification(updatedForm);
          mailer.submissionConfirmation(updatedForm);
        }
        break;
      case "RESUBMITTAL": // is this an edit or a submit? Or both? Classify as edit for now
        if (formConfig.onEdit) {
          if (formConfig.onEdit(formUpdate, access)) {
            mailer.resubmissionNotification(updatedForm);
            mailer.resubmissionConfirmation(updatedForm);
          }
        } else {
          mailer.resubmissionNotification(updatedForm);
          mailer.resubmissionConfirmation(updatedForm);
        }
        break;
      case "EDIT":
        if (updatedForm.status !== "DRAFT") {
          if (formConfig.onEdit) {
            if (formConfig.onEdit(formUpdate, access) && !disableNotify) {
              mailer.editNotification(updatedForm, access);
            }
          } else if (!disableNotify) {
            mailer.editNotification(updatedForm, access);
          }
        }
        break;
      case "COMMENT":
        if (formConfig.onComment) {
          if (formConfig.onComment(formUpdate, access) && !disableNotify) {
            mailer.commentNotification(updatedForm, access);
          }
        } else if (!disableNotify) {
          mailer.commentNotification(updatedForm, access);
        }
        break;
      case "ARCHIVING":
        updatedForm = await addSystemEvent(
          formUpdate.form,
          {
            action: "Archived",
            agent: access.name,
            role: "Administrator",
            description: "Archived the form",
            color: "rgb(222, 147, 0)",
          },
          1000
        );
        break;
      case "EXTERNAL_ID":
        updatedForm = await addSystemEvent(
          formUpdate.form,
          {
            action: "Ext. ID",
            agent: access.name,
            role: "Administrator",
            description: "Set an external id",
            color: "rgb(222, 147, 0)",
          },
          1000
        );
        break;
    }
    res.status(200).send(updatedForm);
  } catch (error) {
    res.status(error.code).send();
  }
});

// Returns pending forms for the current user email somewhere in the supervisor chain. max 100 (sorted by submissiondate)
router.get("/supervisorCases/?*", async (req, res) => {
  const { query: expressQuery, access } = req;
  const status = expressQuery.status as System.FormStatus;
  const result: {
    [key in System.FormType]: System.Form[];
  } = getEmptyFormTypeObject<System.Form[]>([]);
  if (!access.email) {
    // don't perform search on empty email address
    return res.status(200).send(result);
  }
  for (const formConfig of getFormConfigs()) {
    const andQuery: any[] = [
      {
        supervisorActions: {
          $elemMatch: { email: { $regex: access.email, $options: "i" } },
        },
      },
    ];
    if (status) {
      andQuery.push({ status });
    }
    const resp = await (formConfig.model as Model<FormDocument>)
      .find({
        $and: andQuery,
      })
      .sort({ submissionDate: -1 })
      .limit(100);
    result[formConfig.formType] = resp;
  }
  return res.status(200).send(result);
});

// Returns all forms for the current user email with the status DRAFT
router.get("/userDrafts/?*", async (req, res) => {
  const { access } = req;
  const result: {
    [key in System.FormType]: System.Form[];
  } = getEmptyFormTypeObject<System.Form[]>([]);
  if (!access.email) {
    // don't perform search on empty email address
    return res.status(200).send(result);
  }

  for (const formConfig of getFormConfigs()) {
    const resp = await (formConfig.model as Model<FormDocument>)
      .find({
        $and: [
          { submitterEmail: { $regex: access.email, $options: "i" } },
          { status: "DRAFT" },
        ],
      })
      .sort({ submissionDate: -1 })
      .limit(10);
    result[formConfig.formType] = resp;
  }
  return res.status(200).send(result);
});

// Returns all forms for the current user email for a given year (pagination doesn't work because of multi collection query)
router.get("/userCases/?*", async (req, res) => {
  const { access, query } = req;
  const result: {
    [key in System.FormType]: System.Form[];
  } = getEmptyFormTypeObject<System.Form[]>([]);
  if (!access.email) {
    // don't perform search on empty email address
    return res.status(200).send(result);
  }
  const fromDate = new Date(query.fromDate as string);
  const toDate = new Date(query.toDate as string);

  for (const formConfig of getFormConfigs()) {
    const resp = await (formConfig.model as Model<FormDocument>)
      .find({
        $and: [
          { submitterEmail: { $regex: access.email, $options: "i" } },
          { submissionDate: { $gt: fromDate } },
          { submissionDate: { $lt: toDate } },
        ],
      })
      .sort({ submissionDate: -1 })
      .limit(100);
    result[formConfig.formType] = sanitizeForms(
      resp,
      formConfig.hideApprovedForms ? "BASIC" : "DEFAULT"
    );
  }

  return res.status(200).send(result);
});

// Delete all drafts for the user
router.delete("/userDrafts/", async (req, res) => {
  const { access } = req;
  let deleteCount = 0;
  for (const formConfig of getFormConfigs()) {
    const result = await (formConfig.model as Model<FormDocument>).deleteMany({
      $and: [
        { submitterEmail: { $regex: access.email, $options: "i" } },
        { status: "DRAFT" },
      ],
    });
    deleteCount += result.deletedCount || 0;
  }
  return res.status(200).send({ deleteCount });
});

// Delete a specific form
router.delete("/", async (req, res) => {
  const { body, access } = req;
  const id = body.id as string;
  const formType = body.formType as System.FormType;
  try {
    const formAccess = await getFormAccess(formType, id, access);
    if (!formAccess.canDelete) {
      return res.status(403).send();
    }
    const formConfig = getFormConfig(formType);
    const result = await formConfig.model.deleteOne({ _id: id });
    return res.status(200).send(result);
  } catch (error) {
    res.status(error.code).send();
  }
});

// Get a specific form
router.get("/", async (req, res) => {
  const { query, access } = req;
  const id = query.id as string;
  const formType = query.formType as System.FormType;
  try {
    const formAccess = await getFormAccess(formType, id, access);
    const sanitizedForm = sanitizeForm(formAccess);
    if (!sanitizedForm) {
      // returns undefined if there's no read access
      res.status(403).send();
      return;
    }
    res.status(200).send(sanitizedForm);
  } catch (error) {
    console.error(error);
    res.status(403).send();
  }
});

// Special route for lab-booking
router.get("/lab-booking/:id", async (req, res) => {
  const externalId = req.params.id;
  const forms = (await getFormConfig("RISK_ASSESSMENT").model.find({
    externalId,
  })) as System.Form[];
  if (!forms || forms.length === 0) {
    return res.status(200).send([]);
  }
  res.status(200).send(sanitizeForms(forms, "MINIMAL"));
});

// Search, exclusively an admin feature
router.get("/search/?*", async (req, res) => {
  const { query: expressQuery, access } = req;

  const currentDate = new Date();
  const oneYearAgo = new Date(
    currentDate.getFullYear() - 1,
    currentDate.getMonth()
  );
  try {
    const formType = expressQuery.formType as System.FormType;
    const fromDate = expressQuery.fromDate
      ? new Date(expressQuery.fromDate as string)
      : oneYearAgo;
    const toDate = expressQuery.toDate
      ? new Date(expressQuery.toDate as string)
      : currentDate;
    const status = (expressQuery.status as System.FormStatus) || "Any";
    const type = (expressQuery.type as System.ArchiveSearchType) || "DEFAULT";
    const customField = expressQuery.customField as string;
    const query = expressQuery.query || ("" as string);
    const page = parseInt(expressQuery.page as string, 10) || 1;
    const limit = parseInt(expressQuery.limit as string, 10) || 10;
    const includeArchived = expressQuery.includeArchived === "true";
    const formConfig = getFormConfig(formType);
    if (!access.admin[formType]) {
      return res.status(403).send();
    }
    const options = {
      page,
      limit,
      sort: { submissionDate: -1 },
    };
    let result;
    const andQuery: any = [
      { submissionDate: { $gt: fromDate } },
      { submissionDate: { $lt: toDate } },
      { status: { $ne: "DRAFT" } },
    ];
    if (status !== "Any") {
      andQuery.push({ status });
    }
    if (!includeArchived) {
      andQuery.push({ isArchived: { $ne: true } });
    }
    // different queries depending on search type
    // Comment does a text search on comment (requires text index on collection)
    if (type === "COMMENT") {
      // don't perform text search if query is an empty string
      if (!query) {
        result = await (formConfig.model as any).paginate(
          {
            $and: andQuery,
          },
          options
        );
        return res.status(200).send(result);
      }
      result = await (formConfig.model as any).paginate(
        {
          $text: { $search: query },
          $and: andQuery,
        },
        options
      );
      return res.status(200).send(result);
    }
    if (type === "CUSTOM_FIELD") {
      const q: any = {
        $and: andQuery,
      };
      q[customField] = { $regex: query, $options: "i" };
      result = await (formConfig.model as any).paginate(q, options);
      return res.status(200).send(result);
    }
    // Default matches on (partial): id, submitter/supervisors name/email.
    if (type === "DEFAULT") {
      result = await (formConfig.model as any).paginate(
        {
          $or: [
            { _id: { $regex: query } },
            { externalId: { $regex: query, $options: "i" } },
            { submitterEmail: { $regex: query, $options: "i" } },
            { submitterName: { $regex: query, $options: "i" } },
            {
              supervisorActions: {
                $elemMatch: { email: { $regex: query, $options: "i" } },
              },
            },
            {
              supervisorActions: {
                $elemMatch: { name: { $regex: query, $options: "i" } },
              },
            },
          ],
          $and: andQuery,
        },
        options
      );
      return res.status(200).send(result);
    }
  } catch (err) {
    res.status(err.message).send();
  }
});
export const sanitizeForms = (
  forms: System.Form[],
  access: System.FormReadAccess
): System.Form[] => {
  return forms.map((form) => {
    return sanitizeForm({ form, readAccess: access });
  });
};
/**
 * Filter out certain info in froms based on readAccess. E.g. DEFAULT typically refers to the submitter, who should not be able to see e.g. supervisor comments.
 */
export const sanitizeForm = (formAccess: {
  form?: System.Form;
  readAccess: System.FormReadAccess;
}) => {
  const form = formAccess.form.toObject();
  if (!form) {
    return undefined;
  }
  const {
    _id,
    externalId,
    submitterName,
    submitterEmail,
    submissionDate,
    status,
    formType,
  } = form;

  switch (formAccess.readAccess) {
    case "NONE":
      return undefined;
    // Only enough to show it as a table row (possibly excluding the summary())
    case "MINIMAL":
      return {
        _id,
        externalId,
        formType,
        submissionDate,
        status,
      };
    case "BASIC":
      return {
        _id,
        externalId,
        formType,
        submitterName,
        submitterEmail,
        submissionDate,
        status,
        supervisorActions: [getLatestSupervisorAction(form)],
      } as System.Form;
    // Stanard for a submitter - can see everything they submitted and supervisors, but not edits or comments
    case "DEFAULT":
      const { edits, comments, systemEvents, ...rest } = form;
      return {
        ...rest,
        edits: [],
        comments: [],
        systemEvents: [],
      } as System.Form;
    // Standard for an admin - can see everything (assuming it has been submitted)
    case "FULL":
      return form;
  }
};
