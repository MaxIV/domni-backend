import express from "express";
import { isADAdmin } from "../utilities/Helpers";
import { search, update } from "../ldap";
import { LDAPPerson } from "../models/mongoSchema";

export const router = express.Router();

router.get("/person/?*", async (req, res) => {
  const ldapRequest = req.body;
  const { access, query } = req;
  const email = query.email as string;
  try {
    const result = await LDAPPerson.findOne({email});
    return res.status(200).send(result);
  } catch (error) {
    console.log(error);
    return res.status(500).send(error);
  }
});

router.post("/", async (req, res) => {
  const ldapRequest = req.body;
  try {
    const result = await search(ldapRequest);
    return res.status(200).send(result);
  } catch (error) {
    console.log(error);
    return res.status(500).send(error);
  }
});
router.put("/", async (req, res) => {
  const { access } = req;
  if (!isADAdmin(access)) {
    return res.status(403).send();
  }
  const person = req.body as System.LDAPPerson;
  try {
    update(person, (err, result) => {
      if (!err) {
        return res.status(200).send(result);
      }
      return res.status(500).send(err);
    });
  } catch (error) {
    console.log(error);
    return res.status(500).send(error);
  }
});
