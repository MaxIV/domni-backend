import express from "express";
import multer from "multer";
import fs from "fs";
import path from "path";

export const router = express.Router();

router.post("/", async function (req, res) {
  const stamp = Date.now();
  const storage = multer.diskStorage({
    destination(reqD, file, cb) {
      cb(null, "files");
    },
    filename(reqF, file, cb) {
      cb(null, stamp + "-" + file.originalname); // New name with timestamp for uniqueness
    },
  });
  const upload = multer({ storage }).array("file");
  try {
    upload(req, res, function (err: any) {
      if (err) {
        return res.status(500).json(err);
      }
      return res.status(200).json(stamp); // Send timestamp to make corresponding name change in frontend
    });
  } catch (err) {
    console.log("POST of files failed, error: " + err);
    return res.status(500).json(err);
  }
});

router.get("/:id", async function (req, res) {
  try {
    // Preventing directory traversal attacks:
    const acceptedPath = path.resolve("./files");
    const incomingPath = path
      .resolve("./files/" + req.params.id)
      .slice(0, acceptedPath.length);
    if (incomingPath !== acceptedPath) {
      return res
        .status(403)
        .send("Chosen file is not within authorized directories.");
    }
    fs.readFile("./files/" + req.params.id, function (err, data) {
      if (err !== null) {
        console.log("file read error", err);
      }
      return res.status(200).send(data);
    });
  } catch (err) {
    console.log("File GET error: " + err);
    return res.status(500).json(err);
  }
});
