import express from "express";
export const router = express.Router();

router.get("/dbData", async (req, res) => {
  const type:System.DbType = process.env.MONGO.includes("domni-cypress")
    ? "TEST"
    : process.env.MONGO.includes("forms-test")
    ? "DEVELOPMENT"
    : "PRODUCTION";
  return res.status(200).send({ type });
});
