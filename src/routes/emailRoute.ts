import express from "express";
import * as mailer from "../utilities/mailer";
import * as mongoSchema from "../models/mongoSchema";

export const router = express.Router();
router.post("/", async function (req, res) {
  const simpleForm = req.body.simpleForm as System.SimpleForm;
  const { emailSubject, emailRecipient, data } = simpleForm;
  try {
    mailer.simpleForm(emailSubject, emailRecipient, data);
    const result = await mongoSchema.SimpleForm.create(simpleForm);
    console.log("Created simpleform " + result.id);
    return res.status(200).send();
  } catch (err) {
    console.log("POST FAILED", err);
    return res.status(500).json(err);
  }
});
