import express from "express";
import {
  addCollection,
  getCollection,
  removeCollection,
  updateCollection,
} from "../database/collection";
import { List, Project, Supervisor } from "../models/mongoSchema";
export const router = express.Router();

/**
 * This router covers 3 different database collections - List, Supervisor and Project. They've been merged into one due to heavy code duplication. This may prove to be a mistake.
 * @param req
 */
const resolveCollection = (collectionType:  System.CollectionType) => {
  switch (collectionType) {
    case "LIST":
      return List;
    case "SUPERVISOR":
      return Supervisor;
    case "PROJECT":
      return Project;
    default:
      console.error(
        "Unknown or missing collectionType: " + collectionType
      );
  }
};
router.get("/", async (req, res) => {
  try {
    const data = await getCollection(
      resolveCollection(req.query.collectionType as System.CollectionType),
      req.query.form as System.FormType
    );
    if (data) {
      return res.status(200).send(data);
    } else {
      return res.status(500);
    }
  } catch (err) {
    return res.status(500).json(err);
  }
});

router.post("/", async  (req, res) => {
  const { body, access } = req;
  const {collectionType, collection} = body;
  if (!access.admin[collection.form as System.FormType]) {
    return res.status(403).send();
  }
  try {
    const listWithID = await addCollection(resolveCollection(collectionType as System.CollectionType), collection);
    return res.status(200).send(listWithID);
  } catch (err) {
    return res.status(500).send();
  }
});

router.delete("/", async  (req, res) => {
  const { body, access } = req;
  const { _id, formType, collectionType } = body;
  if (!_id || !formType) {
    return res.status(400).json("Required params: _id, formType");
  }
  if (!access.admin[formType as System.FormType]) {
    return res.status(403).send();
  }
  try {
    const deleted = await removeCollection(resolveCollection(collectionType as System.CollectionType), _id);
    return res.status(200).send(deleted);
  } catch (err) {
    return res.status(500).send();
  }
});

router.put("/", async (req, res) => {
  const { body, access } = req;
  const { collection, collectionType } = body;
  if (!access.admin[(collection as System.Collection).form]) {
    return res.status(403).send();
  }
  try {
    const updated = await updateCollection(resolveCollection(collectionType as System.CollectionType), collection);
    return res.status(200).send(updated);
  } catch (err) {
    return res.status(500).send();
  }
});
