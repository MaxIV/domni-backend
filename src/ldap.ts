import * as fs from "fs";
import { createClient, SearchEntry, Change, Client } from "ldapjs";
import { LDAPPerson, Config } from "./models/mongoSchema";

export const update = (
  person: System.LDAPPersonUpdate,
  callback: (err: Error, res: System.LDAPPerson | null) => void
) => {
  getClient((client) => {
    // This is so stupid
    const changes: Change[] = [
      new Change({
        operation: "replace",
        modification: {
          displayName: person.fullName,
        },
      }),
      new Change({
        operation: "replace",
        modification: {
          title: person.title,
        },
      }),
      new Change({
        operation: "replace",
        modification: {
          physicalDeliveryOfficeName: person.office,
        },
      }),
      new Change({
        operation: "replace",
        modification: {
          telephoneNumber: person.phone,
        },
      }),
      new Change({
        operation: "replace",
        modification: {
          mail: person.email,
        },
      }),
    ];

    client.modify(person.DN, changes, async (err) => {
      if (!err) {
        const updated = await LDAPPerson.findOneAndUpdate(
          { DN: person.DN },
          { ...person },
          { new: true, upsert: false }
        );
        callback(null, updated);
        console.log("updated user:", updated);
      } else {
        callback(err, null);
        console.log("Error updating ldap:", err);
      }
    });
  });
};
export const search = async (request: any) => {
  if (!request.attribute || !request.searchQuery) {
    return await LDAPPerson.find({});
  }
  const { attribute, searchQuery } = request;
  const q: any = {};
  q[attribute] = { $regex: searchQuery, $options: "i" };
  return await LDAPPerson.find(q);
};

/**
 * Performs a full search, delete existing ldapPeople, insert the new ones, creates profile files and updates ldapTimestamp
 */
export const cache = async (afterDbUpdate?: () => void) => {
  getClient((client) => {
    const people: System.LDAPPerson[] = [];
    client.search(
      "CN=Users,DC=maxlab,DC=lu,DC=se",
      {
        filter: "memberOf=CN=Staff,CN=Users,DC=maxlab,DC=lu,DC=se",
        scope: "sub",
        timeLimit: 6000,
      },
      (err, res) => {
        res.on("searchEntry", function (entry) {
          const converted = convert(entry);
          const imgBuffer = Buffer.from(converted.jpegPhoto || "", "binary");
          const person: System.LDAPPerson = {
            fullName: (entry.object.displayName as string) || "",
            firstName: (entry.object.givenName as string) || "",
            title: (entry.object.title as string) || "",
            office: (entry.object.physicalDeliveryOfficeName as string) || "",
            phone: (entry.object.telephoneNumber as string) || "",
            memberOf: parseMembership(entry),
            username: (entry.object.sAMAccountName as string) || "",
            email: (entry.object.mail as string) || "",
            DN: (entry.object.distinguishedName as string) || "",
            jpeg: converted.jpegPhoto
              ? `/api/files/${entry.object.sAMAccountName}.jpeg`
              : "",
          };
          fs.writeFile(`./files/${person.username}.jpeg`, imgBuffer, (e) => {
            if (e) {
              console.log("error saving profile picture", e);
            }
          });
          people.push(person);
        });
        res.on("end", async function (result) {
          client.unbind();
          if (people.length > 0) {
            await LDAPPerson.deleteMany({});
            await LDAPPerson.insertMany(people);
            const config = await Config.findOneAndUpdate(
              {},
              { ldapCacheTimestamp: new Date() },
              { new: true, upsert: true }
            );
            console.log("updated. New config:", config);
            if (afterDbUpdate) {
              afterDbUpdate();
            }
          }
        });
        res.on("error", function (resErr) {
          console.log("ldap error", resErr);
          client.unbind();
        });
      }
    );
  });
};
export const getClient = (callback?: (client: Client) => void) => {
  const client = createClient({
    url: process.env.LDAP_SERVER,
  });
  client.bind(
    process.env.LDAP_USERNAME,
    process.env.LDAP_PASSWORD,
    function (err, res) {
      if (err) {
        console.log("ldap bind error", err);
      }
      if (callback) {
        callback(client);
      }
    }
  );
};

const parseMembership = (entry: SearchEntry): string[] => {
  let memberOf = entry.object.memberOf || [];
  if (!Array.isArray(memberOf)) {
    memberOf = [memberOf];
  }
  const groups = memberOf
    .map((group) => group.split(","))
    .reduce((all, curr) => [...all, ...curr], [])
    .map((e) => e.split("="))
    .filter((pair) => pair[0] === "CN")
    .map((pair) => pair[1]);
  return groups.filter((elem, i) => groups.indexOf(elem) === i);
};

function convert(entry: SearchEntry): any {
  const obj: any = {
    dn: entry.dn.toString(),
    controls: [],
  };
  entry.attributes.forEach(function (a: any) {
    const item = a.buffers;
    if (item && item.length) {
      if (item.length > 1) {
        obj[a.type] = item.slice();
      } else {
        obj[a.type] = item[0];
      }
    } else {
      obj[a.type] = [];
    }
  });
  entry.controls.forEach(function (element: any) {
    obj.controls.push(element.json);
  });
  return obj;
}
