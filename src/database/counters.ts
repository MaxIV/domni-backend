import * as mongoSchema from "../models/mongoSchema";
import { getFormTypes } from "../config";

// Gets next PO number
export const getNext = async (form:System.FormType):Promise<string> => {
  try {
    const sequence = await mongoSchema.Counter.findByIdAndUpdate(form, {
      $inc: { sequenceValue: 1 },
    });
    return sequence.sequenceValue.toString();
  } catch (err) {
    console.log("Could not get next counter for " + form + " , error: " + err);
  }
};

// Creates new counter if none was found
export const initialize = async () => {
  const createIfNeeded = async (formType:System.FormType) => {
    try {
      const counter = await mongoSchema.Counter.findById(formType);
      if (!counter) {
        console.log(`Creating counter for ${formType}`);
        mongoSchema.Counter.create({
          _id: formType,
          sequenceValue: 10000,
        });
      }
    } catch (err) {
      console.log("Initialization of counter failed, error: " + err);
    }
  };

  getFormTypes().forEach((formType) => {
    createIfNeeded(formType);
  });
};

