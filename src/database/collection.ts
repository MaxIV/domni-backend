import mongoose from "mongoose";
import {
  ProjectDocument,
  SupervisorDocument,
  ListDocument,
} from "../models/mongoSchema";
type collectionDocument = ProjectDocument | SupervisorDocument | ListDocument;

export const addCollection = async(
  model: mongoose.Model<collectionDocument>,
  collection: System.List | System.Supervisor | System.Project
) => {
  try {
    return await model.create(collection);
  } catch (err) {
    console.log("collection add error", err)
    const error:System.APIError = {code: 500}
    throw error;
  }
}

export const removeCollection = async (
  model: mongoose.Model<collectionDocument>,
  _id: string
) => {
  try {
    return await model.findOneAndRemove({ _id });
  } catch (err) {
    const error:System.APIError = {code: 500}
    throw error;
  }
};

export const getCollection = async (
  model: mongoose.Model<collectionDocument>,
  form: System.FormType
) => {
  try {
    return (await form) ? await model.find({ form }) : await model.find({});
  } catch (err) {
    const error:System.APIError = {code: 500}
    throw error;
  }
};

export const updateCollection = async (
  model: mongoose.Model<collectionDocument>,
  collection: (System.List | System.Supervisor | System.Project) & System.withId
) => {
  try {
    return await model.findByIdAndUpdate(collection._id, collection, {
      new: true,
    });
  } catch (err) {
    const error:System.APIError = {code: 500}
    throw error;
  }
};
