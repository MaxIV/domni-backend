import * as mongoSchema from "../models/mongoSchema";
import { getFormConfig, getFormTypes } from "../config";

// Clears database
export const clear = async () => {
  console.log("Clearing database...");
  await mongoSchema.Counter.deleteMany({});
  await mongoSchema.Supervisor.deleteMany({});
  await mongoSchema.Project.deleteMany({});
  getFormTypes().forEach(async (formType) => {
    try {
      await  getFormConfig(formType).model.deleteMany({});
    } catch (errror) {
      console.log("Failed clearing db for " + formType);
    }
  });
};
export const clearCounter  = async (form?:System.FormType) => {
  if (form){
    await mongoSchema.Counter.deleteOne({_id: form});
  }else{
    await mongoSchema.Counter.deleteMany({});
  }
}
export const clearLDAPPeople = async () => {
  await mongoSchema.LDAPPerson.deleteMany({})
  await mongoSchema.Config.deleteMany({})
}
export const clearProjectsSupervisorsAndCustomLists = async (form?:System.FormType) => {
  if (form){
    await mongoSchema.Supervisor.deleteMany({form});
    await mongoSchema.Project.deleteMany({form});
    await mongoSchema.List.deleteMany({form})
  }else{
    await mongoSchema.Supervisor.deleteMany({});
    await mongoSchema.Project.deleteMany({});
    await mongoSchema.List.deleteMany({})
  }

}

// Delete all forms
export const deleteForms = async (formType:System.FormType) => {
    await getFormConfig(formType).model.deleteMany({});
};
// Delete all forms
export const deleteAllForms = async () => {
  getFormTypes().forEach(async (formType) => {
    await getFormConfig(formType).model.deleteMany({});
  });
};
