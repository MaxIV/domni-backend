import { getFormConfigs, getFormTypes } from "../config";
import fetch from "node-fetch";

export async function auth(jwtToken: string): Promise<System.Access> {
  const admin: any = {};
  getFormTypes().forEach((formType) => (admin[formType] = false));
  const access: System.Access = {
    authenticated: false,
    admin,
    name: "",
    username: "",
    email: "",
    groups: [],
  };
  try {
    const result = await fetch(process.env.DECODE_URL || "https://auth.maxiv.lu.se/v1/decode", {
      method: "POST",
      body: JSON.stringify({
        jwt: jwtToken,
      }),
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
    });
    if (!result.ok) {
      return access;
    }
    const jwtDecoded = (await result.json()) as System.JWT;
    if (process.env.SPOOF_NAME) {
      jwtDecoded.name = process.env.SPOOF_NAME;
    }
    if (process.env.SPOOF_USERNAME) {
      jwtDecoded.username = process.env.SPOOF_USERNAME;
    }
    if (process.env.SPOOF_EMAIL) {
      jwtDecoded.email = process.env.SPOOF_EMAIL;
    }
    if (process.env.SPOOF_GROUPS) {
      jwtDecoded.groups = process.env.SPOOF_GROUPS.split(" ");
    }
    access.groups = jwtDecoded.groups;
    access.authenticated = true;
    access.name = jwtDecoded.name;
    access.username = jwtDecoded.username;
    access.email = jwtDecoded.email;
    getFormConfigs().forEach((config) => {
      access.admin[config.formType] =
        jwtDecoded.groups.includes("Information Management") ||
        config.adminGroups.some((group) => jwtDecoded.groups.includes(group)) ||
        config.adminUsers.includes(access.username);
    });
    return access;
  } catch (error) {
    return access;
  }
}
