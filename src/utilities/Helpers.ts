import { ScheduledEvent } from "../models/mongoSchema";
import { getFormConfig, getFormTypes } from "../config";
import * as constants from "../constants";
import * as mailer from "./mailer";
/** Returns true if the user is an AD admin (i.e. is allowed to update AD users). */
export const isADAdmin = (access: System.Access) => {
  return access.groups.includes(constants.AD_GROUP_AD_ADMIN);
};

export const createSafetyTestReminders = async (
  formUpdate: System.FormUpdate
) => {
  const current = new Date();
  const oneMonthBeforeExpiry = new Date(
    current.getFullYear() + 1,
    current.getMonth() - 1,
    current.getDate(),
    current.getHours()
  );
  const oneWeekBeforeExpiry = new Date(
    current.getFullYear() + 1,
    current.getMonth(),
    current.getDate() - 7,
    current.getHours()
  );
  const oneDayBeforeExpiry = new Date(
    current.getFullYear() + 1,
    current.getMonth(),
    current.getDate() - 1,
    current.getHours()
  );
  // const debug = new Date(
  //   current.getFullYear(),
  //   current.getMonth(),
  //   current.getDate(),
  //   current.getHours(),
  //   current.getMinutes() + 1,
  // );
  await ScheduledEvent.deleteMany({
    type: "SAFETY_TEST_REMINDER",
    "payload.email": formUpdate.form.submitterEmail,
  });
  await ScheduledEvent.create([
    {
      type: "SAFETY_TEST_REMINDER",
      executionTime: oneMonthBeforeExpiry,
      payload: {
        email: formUpdate.form.submitterEmail,
        name: formUpdate.form.submitterName,
        reminderCount: 1,
      },
    } as System.SafetyTestReminderEvent,
    {
      type: "SAFETY_TEST_REMINDER",
      executionTime: oneWeekBeforeExpiry,
      payload: {
        email: formUpdate.form.submitterEmail,
        name: formUpdate.form.submitterName,
        reminderCount: 2,
      },
    } as System.SafetyTestReminderEvent,

    {
      type: "SAFETY_TEST_REMINDER",
      executionTime: oneDayBeforeExpiry,
      payload: {
        email: formUpdate.form.submitterEmail,
        name: formUpdate.form.submitterName,
        reminderCount: 3,
      },
    } as System.SafetyTestReminderEvent,
  ]);
  addSystemEvent(
    formUpdate.form,
    {
      action: "Scheduled reminders",
      agent: "Domni",
      role: "System",
      description: "Scheduled email reminders for next year.",
      color: "black",
    },
    10000
  );
};

/**
 * Resolves access for the current user to a specific form. It considers the users role (submitter, supervisor, admin) and the status of the form (draft, pending or rejected) to determine read, write and delete access
 */
export const getFormAccess = async (
  formType: System.FormType,
  _id: string,
  access: System.Access
): Promise<System.FormAccess> => {
  const form = (await getFormConfig(formType).model.findOne({
    _id,
  })) as System.Form;

  if (!form) {
    return {
      readAccess: "NONE",
      canEdit: false,
      canResolve: false,
      canDelete: false,
      isStaff: false,
      isSubmitter: false,
      isSupervisor: false,
      isAdmin: false,
    };
  }
  const formAccess = getFormAccessInner(form, { ...access, phone: "" }, "");
  formAccess.form = form;
  return formAccess;
};

/**
 * Separated so that this can be identical to Helper.getFormAccess in the frontend
 */
export const getFormAccessInner = (
  form: System.Form,
  user: System.User,
  context: string // there are just so many calls and so many bugs related to this, that context help debugging a lot
): System.FormAccess => {
  const { status, submitterEmail, supervisorActions, formType } = form;
  if (!user.email) {
    return returnAccess(
      {
        readAccess: "NONE",
        canEdit: false,
        canResolve: false,
        canDelete: false,
        isStaff: false,
        isSubmitter: false,
        isSupervisor: false,
        isAdmin: false,
      },
      context
    );
  }
  const isSubmitter = !!(
    user.email &&
    submitterEmail &&
    submitterEmail.toLowerCase() === user.email.toLowerCase()
  );
  const isSupervisor = !!(
    user.email &&
    supervisorActions
      .map((supervisor) => supervisor.email.toLowerCase())
      .includes(user.email.toLowerCase())
  );
  const isAdmin = user.admin[formType];
  const formAccess: System.FormAccess = {
    readAccess: "NONE",
    canEdit: false,
    canResolve: false,
    canDelete: false,
    isStaff: isStaff(user),
    isSubmitter,
    isSupervisor,
    isAdmin,
  };
  if (!form._id) {
    return returnAccess(
      { ...formAccess, readAccess: "FULL", canEdit: true },
      context
    );
  }
  if (status === "DELETED") {
    return returnAccess(formAccess, context);
  }
  // If draft, allow owner to do anything, and don't allow anyone else to do anything.
  if (status === "DRAFT" && isSubmitter) {
    return returnAccess(
      {
        ...formAccess,
        readAccess: "FULL",
        canEdit: true,
        canDelete: true,
      },
      context
    );
  } else if (status === "DRAFT" && !isSubmitter) {
    return returnAccess(
      {
        ...formAccess,
        readAccess: "NONE",
        canEdit: false,
        canDelete: false,
      },
      context
    );
  }
  const formConfig = getFormConfig(formType);
  if(formConfig.approvedFormsPublic && status === "APPROVED") {
    return returnAccess(
      {...formAccess, readAccess: "FULL", canResolve: true},
      context
    )
  }
  formAccess.readAccess =
    isSupervisor || isAdmin ? "FULL" : isSubmitter ? "DEFAULT" : "NONE";
  if (
    isSubmitter &&
    !isAdmin &&
    formConfig.hideApprovedForms &&
    status === "APPROVED"
  ) {
    formAccess.readAccess = "BASIC";
  }
  if (form.isArchived) {
    return returnAccess(
      { ...formAccess, canResolve: false, canEdit: false, canDelete: false },
      context
    );
  }
  formAccess.canResolve =
    (isAdmin || isSupervisor) &&
    form.status !== "REJECTED" &&
    (form.status !== "APPROVED" || isAdmin);

  formAccess.canEdit =
    (isAdmin && formConfig.canEditAdmin && status === "PENDING") ||
    (isSupervisor && formConfig.canEditSupervisor && status === "PENDING") ||
    (isSubmitter && status === "REJECTED");

  formAccess.canDelete =
    (isAdmin && status === "REJECTED") || (isSubmitter && status === "DRAFT");
  return returnAccess(formAccess, context);
};

export const isStaff = (user: System.User) => {
  return user.groups.map((group) => group.toLowerCase()).includes("staff");
};


const returnAccess = (formAccess: System.FormAccess, context: string) => {
  const DEBUG = false; // if true, print access before each return statement
  if (DEBUG) {
    console.log("form Access:", context);
    console.log(formAccess);
  }
  return formAccess;
};

/**
 * Returns and object with a key/property for each formType, with defaultData as value. Useful to generate e.g. initial state data for supers and projects
 * @param formFilterProps defaults to no filtering of omitted
 */

export function getEmptyFormTypeObject<T>(defaultData: T): {
  [key in System.FormType]: T;
} {
  const result: any = {};
  getFormTypes().forEach((formType) => {
    result[formType] = Array.isArray(defaultData)
      ? defaultData.slice(0)
      : defaultData;
  });
  return result;
}

export const processSafetyTestReminderEvents = async () => {
  const cutoffDate = new Date();
  cutoffDate.setDate(cutoffDate.getDate() + 1);
  const events = await ScheduledEvent.find({
    type: "SAFETY_TEST_REMINDER",
    executionTime: { $lt: cutoffDate },
  });
  events.forEach((event) => {
    try {
      mailer.safetyTestReminder(event);
      event.remove();
    } catch (error) {
      console.error(error);
    }
  });
};
export const getOldPendingForms = async (formType: System.FormType) => {
  const cutoff = new Date();
  const currentDate = new Date();
  cutoff.setDate(cutoff.getDate() - 3);
  return await getFormConfig(formType).model.aggregate([
    {
      $addFields: {
        latestAction: {
          $arrayElemAt: ["$supervisorActions", -1],
        },
      },
    },
    {
      $match: {
        status: "PENDING",
        "latestAction.timestamp": { $lt: cutoff },
        "latestAction.action": "PENDING",
        $or: [
          { postponedUntil: { $lt: currentDate } },
          { postponedUntil: null },
        ],
      },
    },
  ]);
};

export const getLatestSupervisorAction = (form: System.Form) => {
  return form.supervisorActions.sort((a, b) =>
    a.timestamp < b.timestamp ? 1 : -1
  )[0];
};

export const getOldestSupervisorAction = (form: System.Form) => {
  return form.supervisorActions.sort((a, b) =>
    a.timestamp > b.timestamp ? 1 : -1
  )[0];
};

/**
 * The oldest supervisor is at [0]
 * @param form
 */
export const getSortedSupervisorActions = (
  form: System.Form,
  reversed = false
): System.SupervisorAction[] => {
  const sign = reversed ? -1 : 1;
  return form.supervisorActions.sort((a, b) =>
    a.timestamp < b.timestamp ? -1 * sign : 1 * sign
  );
};

/**
 * Sorted - The oldest supervisor is at [0]
 * @exclude email of supervisor to exclude from the list
 */
export const getListeningSupervisors = (
  form: System.Form,
  exclude?: string
) => {
  return getSortedSupervisorActions(form).filter(
    (supervisor) =>
      supervisor.notify &&
      (exclude
        ? supervisor.email.toLowerCase() !== exclude.toLowerCase()
        : true)
  );
};

export const formatDbKeyForEmail = (key: string) => {
  // Some special cases:
  switch (key) {
    case "__v":
      return "Revision";
    case "_id":
      return "Form ID";
  }
  return formatCamelCase(key);
};
const formatCamelCase = (camelCase: string) => {
  const match = camelCase.replace(/([A-Z])/g, " $1");
  const words = match.charAt(0).toUpperCase() + match.slice(1);
  return words;
};
export const unwrapJSON = (json: any): any => {
  if (typeof json === "boolean") {
    return json ? "Yes" : "No";
  }
  if (typeof json === "number" || typeof json === "string") {
    return json;
  }
  if (Array.isArray(json)) {
    return (
      "<ul style='padding-left: 1em'>" +
      json
        .map(
          (elem) =>
            "<li style='margin-bottom: 1em'>" + unwrapJSON(elem) + "</li>"
        )
        .join("") +
      "</ul>"
    );
  }
  if (typeof json === "object") {
    return Object.keys(json)
      .map((key) => {
        return formatDbKeyForEmail(key) + ": " + unwrapJSON(json[key]);
      })
      .join("<br/>");
  }
  return "Not specified";
};

/**
 * Logs an action of type 'SystemEvent' for the form 'form'.
 * @param form the form the event is attached to
 * @param event affects how the log event is displayed in the stepper (event widget in the submitted component):
 * action: text inside the square
 * agent and role: rendered under the square
 * description: rendered as tooltip on hover
 * color: color on text and box border
 *
 * @param delayInMillis optional delay in milliseconds on the timestamp of the log event. Useful if it needs to be logged with a timestamp after the default actions, in which case a second (or minute) can be added
 */
export const addSystemEvent = async (
  form: System.Form,
  event: {
    action: string;
    agent: string;
    role: System.EventRole;
    description: string;
    color: string;
  },
  delayInMillis?: number
): Promise<System.Form> => {
  const { action, agent, role, description, color } = event;
  const formConfig = getFormConfig(form.formType);
  const systemEvents = form.systemEvents || [];
  systemEvents.push({
    eventType: "SystemEvent",
    timestamp: new Date(Date.now() + (delayInMillis || 0)),
    action,
    agent,
    role,
    description,
    color,
  });
  const result = await formConfig.model.findByIdAndUpdate(
    form._id,
    { systemEvents },
    {
      new: true,
    }
  );
  console.log(
    `Added systemEvent for ${form.formType} ${result._id}. Action: ${event.action}`
  );
  return result;
};

/**
 *
 * @returns the `config.additionalTexts[emailType]` if it exists, o.w. `undefined`
 */
export const getAdditionalMsg = (
  config: System.FormConfig,
  emailType: System.EmailType,
  form: System.Form
): string | undefined => {
  if (!config.additionalTexts[emailType]) {
    return undefined;
  }
  return config.additionalTexts[emailType](form);
};
