import nodemailer from "nodemailer";
import { SENDER_EMAIL, getFormConfig, formConfigs } from "../config";
import * as Helper from "./Helpers";
import Handlebars from "handlebars";
import fs from "fs-extra";
import { getAdditionalMsg } from "./Helpers";

// Convert camel case to words. camel Case -> Camel Case
Handlebars.registerHelper("propToWord", (props: string) => {
  return Helper.formatDbKeyForEmail(props);
});

Handlebars.registerHelper("unwrapJSON", (props: string) => {
  return Helper.unwrapJSON(props);
});

// Generic mail functions
export async function sendMailMaxIV(
  from: string,
  to: string,
  emailContext: System.EmailContext
) {
  try {
    const { subject } = emailContext;
    process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";
    const transporter = nodemailer.createTransport({
      host: process.env.SMTP_HOST || "mailcatcher.maxiv.lu.se",
      port: parseInt(process.env.SMTP_PORT, 10) || 1025,
      secure: false,
    });
    const htmlTemplate = await fs.readFile(
      "public/templates/emailTemplate.html",
      "utf-8"
    );
    const template = Handlebars.compile(htmlTemplate);
    const email = template(emailContext);
    await transporter.sendMail({ from, to, subject, html: email });
    console.log(`Sent email to ${to}. Subject: ${subject}`);
  } catch (err) {
    console.log("Error sending email", err);
  }
}

/**
 * Sent to submitter upon submission
 */
export async function submissionConfirmation(
  form: System.Form,

) {
  const formConfig = getFormConfig(form.formType);
  const emailContext: System.SubmissionConfirmation = {
    domainName: process.env.DOMAIN_NAME,
    subject: `Confirmation: ${formConfig.name} #${form._id}`,
    submissionConfirmation: {
      formName: formConfig.name,
      supervisorName: Helper.getLatestSupervisorAction(form).name,
      requiresApproval: formConfig.requiresApproval,
      additionalMsg: getAdditionalMsg(formConfig, "SubmissionConfirmation", form),
    },
  };
  await sendMailMaxIV(SENDER_EMAIL, form.submitterEmail, emailContext);
}

/**
 * Sent to the current supervisor upon submission
 */
export async function submissionNotification(
  form: System.Form,

) {
  const formConfig = getFormConfig(form.formType);
  const supervisor = Helper.getLatestSupervisorAction(form);
  const emailContext: System.SubmissionNotification = {
    domainName: process.env.DOMAIN_NAME,
    subject: `Submission: ${formConfig.name}  #${form._id}`,
    submissionNotification: {
      formName: formConfig.name,
      submitterName: form.submitterName,
      formId: form._id,
      link: `${process.env.DOMAIN_NAME}/${formConfig.path}/approval/${form._id}`,
      additionalMsg: getAdditionalMsg(formConfig, "SubmissionNotification", form),
    },
  };
  sendMailMaxIV(SENDER_EMAIL, supervisor.email, emailContext);
}

/**
 * Sent to submitter upon resubmission/revision submission
 */
export async function resubmissionConfirmation(
  form: System.Form,

) {
  const formConfig = getFormConfig(form.formType);
  const emailContext: System.ResubmissionConfirmation = {
    domainName: process.env.DOMAIN_NAME,
    subject: `Confirmation: ${formConfig.name} #${form._id}`,
    resubmissionConfirmation: {
      formName: formConfig.name,
      supervisorName: Helper.getLatestSupervisorAction(form).name,
      additionalMsg: getAdditionalMsg(formConfig, "ResubmissionConfirmation", form),
    },
  };
  await sendMailMaxIV(SENDER_EMAIL, form.submitterEmail, emailContext);
}

/**
 * Sent to the current supervisor upon resubmission/revision submission
 */
export async function resubmissionNotification(
  form: System.Form,

) {
  const formConfig = getFormConfig(form.formType);
  const supervisor = Helper.getLatestSupervisorAction(form);
  const emailContext: System.ResubmissionNotification = {
    domainName: process.env.DOMAIN_NAME,
    subject: `Revision: ${formConfig.name}  #${form._id}`,
    resubmissionNotification: {
      formName: formConfig.name,
      submitterName: form.submitterName,
      formId: form._id,
      link: `${process.env.DOMAIN_NAME}/${formConfig.path}/approval/${form._id}`,
      additionalMsg: getAdditionalMsg(formConfig, "ResubmissionNotification", form),
    },
  };
  sendMailMaxIV(SENDER_EMAIL, supervisor.email, emailContext);
}

/**
 * Sent to all listening supervisors upon edit, except the editor
 * @param editor the person performing the edit
 */
export async function editNotification(
  form: System.Form,
  editor: { name: string; email: string }
) {
  const formConfig = getFormConfig(form.formType);
  Helper.getListeningSupervisors(form, editor.email).forEach((supervisor) => {
    const emailContext: System.EditNotification = {
      domainName: process.env.DOMAIN_NAME,
      subject: `Edit: ${formConfig.name}  #${form._id}`,
      editNotification: {
        formName: formConfig.name,
        editorName: editor.name,
        submitterName: form.submitterName,
        formId: form._id,
        link: `${process.env.DOMAIN_NAME}/${formConfig.path}/approval/${form._id}`,
      },
    };
    sendMailMaxIV(SENDER_EMAIL, supervisor.email, emailContext);
  });
}

/**
 * Sent to the new supervisor upon supervisor reassignment
 */
export async function reassignmentNotification(
  form: System.Form,
  reassigner: { name: string; email: string },
  comment: System.Comment | undefined
) {
  const formConfig = getFormConfig(form.formType);
  const emailContext: System.ReassignmentNotification = {
    domainName: process.env.DOMAIN_NAME,
    subject: `Reassignment: ${formConfig.name}  #${form._id}`,
    reassignmentNotification: {
      formName: formConfig.name,
      submitterName: form.submitterName,
      reassignerName: reassigner.name,
      formId: form._id,
      hasComment: comment && !!comment.msg,
      commentMsg: comment && comment.msg,
      commentTitle: `Reassignment comment by ${reassigner.name}`,
      link: `${process.env.DOMAIN_NAME}/${formConfig.path}/approval/${form._id}`,
    },
  };
  sendMailMaxIV(
    SENDER_EMAIL,
    Helper.getLatestSupervisorAction(form).email,
    emailContext
  );
}

/**
 * Sent to all listening supervisors upon final decision, except decision maker
 */
export async function decisionNotificationListeners(
  form: System.Form,
  decider: { name: string; email: string }
) {
  const formConfig = getFormConfig(form.formType);

  Helper.getListeningSupervisors(form, decider.email).forEach((supervisor) => {
    const emailContext: System.DecisionNotificationListeners = {
      domainName: process.env.DOMAIN_NAME,
      subject: `Decision: ${formConfig.name}  #${form._id}`,
      decisionNotificationListeners: {
        formName: formConfig.name,
        submitterName: form.submitterName,
        formStatus: form.status.toLowerCase(),
        deciderName: decider.name,
      },
    };
    sendMailMaxIV(SENDER_EMAIL, supervisor.email, emailContext);
  });
}

/**
 * Sent to all listening supervisors upon adding of comment, except commentator
 */
export async function commentNotification(
  form: System.Form,
  commentator: { name: string; email: string }
) {
  const formConfig = getFormConfig(form.formType);
  Helper.getListeningSupervisors(form, commentator.email).forEach(
    (supervisor) => {
      const emailContext: System.CommentNotification = {
        domainName: process.env.DOMAIN_NAME,
        subject: `Comment: ${formConfig.name}  #${form._id}`,
        commentNotification: {
          formName: formConfig.name,
          submitterName: form.submitterName,
          commentatorName: commentator.name,
          formId: form._id,
          link: `${process.env.DOMAIN_NAME}/${formConfig.path}/approval/${form._id}`,
        },
      };
      sendMailMaxIV(SENDER_EMAIL, supervisor.email, emailContext);
    }
  );
}

/**
 * Sent to the submitter upon final decision. Not sent in partial decision during supervisor reassignment
 */
export async function decisionNotificationSubmitter(props: {
  form: System.Form;
  decider: { name: string; email: string };
  requiresApproval: boolean;
  comment: System.Comment | undefined;
}) {
  const { form, decider, requiresApproval, comment } = props;
  const formConfig = getFormConfig(form.formType);
  const isRejection = form.status === "REJECTED";
  const emailContext: System.DecisionNotificationSubmitter = {
    domainName: process.env.DOMAIN_NAME,
    subject: `${requiresApproval ? "Decision" : "Processed"}: ${
      formConfig.name
    }  #${form._id}`,
    decisionNotificationSubmitter: {
      formName: formConfig.name,
      formId: form._id,
      formStatus: requiresApproval ? form.status.toLowerCase() : "processed",
      deciderName: decider.name,
      hasComment: comment && !!comment.msg,
      commentMsg: comment && comment.msg,
      commentTitle: isRejection ? `Cause for rejection` : `Approval comment`,
      isRejection,
      link: `${process.env.DOMAIN_NAME}/${formConfig.path}/form/${form._id}`,
      additionalMsg: getAdditionalMsg(formConfig, "DecisionNotificationSubmitter", form),
    },
  };
  sendMailMaxIV(SENDER_EMAIL, form.submitterEmail, emailContext);
}
export const safetyTestReminder = (
  reminder: System.SafetyTestReminderEvent
) => {
  const { name, email, reminderCount } = reminder.payload;
  const firstName = name.split(" ").length > 1 ? name.split(" ")[0] : name;
  const timeEn =
    reminderCount === 1
      ? "one month from now"
      : reminderCount === 2
      ? "one week from now"
      : "tomorrow";
  const timeSv =
    reminderCount === 1
      ? "om en månad"
      : reminderCount === 2
      ? "om en vecka"
      : "i morgon";
  const emailContext: System.SafetyTestReminder = {
    domainName: process.env.DOMAIN_NAME,
    subject: "Reminder: your safety test is about to expire!",
    safetyTestReminder: {
      name: firstName,
      timeSv,
      timeEn,
      link: `${process.env.DOMAIN_NAME}/safety-test/form/`,
    },
  };
  sendMailMaxIV(SENDER_EMAIL, email, emailContext);
};
export async function supervisorReminder() {
  (Object.keys(formConfigs.forms) as System.FormType[]).forEach(
    async (formType) => {
      const oldForms = await Helper.getOldPendingForms(formType);
      const formConfig = getFormConfig(formType);
      oldForms.forEach((form: System.Form) => {
        const emailContext: System.SupervisorReminder = {
          domainName: process.env.DOMAIN_NAME,
          subject: `Reminder: ${formConfig.name}  #${form._id}`,
          supervisorReminder: {
            formName: formConfig.name,
            formId: form._id,
            submitterName: form.submitterName,
            link: `${process.env.DOMAIN_NAME}/${formConfig.path}/approval/${form._id}`,
            profileViewLink: `${process.env.DOMAIN_NAME}/profile`,
          },
        };
        sendMailMaxIV(
          SENDER_EMAIL,
          Helper.getLatestSupervisorAction(form).email,
          emailContext
        );
        Helper.addSystemEvent(form, {
          action: "Reminder",
          agent: "Domni",
          role: "System",
          description: "Send an reminder email",
          color: "black",
        });
      });
    }
  );
}

export const simpleForm = async (
  emailSubject: string,
  emailRecipient: string,
  data: any
) => {
  const emailContext: System.SimpleFormNotification = {
    domainName: process.env.DOMAIN_NAME,
    subject: emailSubject,
    simpleForm: data,
  };
  await sendMailMaxIV("domni@maxiv.lu.se", emailRecipient, emailContext);
};
