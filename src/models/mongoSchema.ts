import mongoose, { Schema } from "mongoose";
import mongoosePaginate from "mongoose-paginate-v2";
import {
  PURCHASE,
  TRAVEL,
  EMPLOYMENT,
  HOTEL,
  CATERING,
  GRANT_INITIATIVE,
  ADDITIONAL_FUNDING,
  SAFETY_TEST,
  RISK_ASSESSMENT,
} from "../constants";

// This contain the properties common to all forms.
const getGenericFormSchema = (formType: System.FormType) => {
  return new mongoose.Schema({
    _id: String,
    externalId: String,
    formType: {
      type: String,
      default: formType,
    },
    submitterName: String,
    submitterEmail: { type: String, index: true },
    submissionDate: { type: Date, index: true },
    supervisorActions: { type: [SupervisorAction], default: [] },
    status: String,
    postponedUntil: Date,
    isArchived: { type: Boolean, default: false, required: true },
    files: { type: [File], default: [] },
    edits: { type: [Edit], default: [] },
    comments: { type: [Comment], default: [] },
    systemEvents: { type: [SystemEvent], default: [] },
  });
};
// Form subcomponents
const SupervisorAction = {
  action: String,
  eventType: {
    type: String,
    default: "SupervisorAction",
  },
  timestamp: {
    type: Date,
    default: new Date(),
  },
  decisionDate: Date,
  name: String,
  email: String,
  comment: String,
  notify: Boolean,
  role: String,
};
const File = {
  name: String,
  size: String,
};
const Edit = {
  eventType: {
    type: String,
    default: "Edit",
  },
  name: String,
  email: String,
  timestamp: {
    type: Date,
    default: new Date(),
  },
  role: String,
};
const SystemEvent = {
  eventType: String,
  timestamp: {
    type: Date,
    default: new Date(),
  },
  action: String,
  agent: String,
  description: String,
  color: String,
  role: String,
};
const Comment = {
  eventType: {
    type: String,
    default: "Comment",
  },
  timestamp: {
    type: Date,
    default: new Date(),
  },
  updated: Date,
  source: String,
  msg: String,
  author: String,
  status: String,
  supervisor: String,
  role: String,
};

// Documents bridging typescript and mongoose models
export interface FormDocument extends System.Form, mongoose.Document {
  _id: string;
}

interface PurchaseFormDocument extends System.PurchaseForm, mongoose.Document {
  _id: string;
}
interface TravelFormDocument extends System.TravelForm, mongoose.Document {
  _id: string;
}
interface EmploymentRequestDocument
  extends System.EmploymentRequest,
    mongoose.Document {
  _id: string;
}
interface GrantInitiativeDocument
  extends System.GrantInitiativeForm,
    mongoose.Document {
  _id: string;
}
interface AdditionalFundingDocument
  extends System.AdditionalFundingForm,
    mongoose.Document {
  _id: string;
}
interface RiskAssessmentDocument
  extends System.RiskAssessmentForm,
    mongoose.Document {
  _id: string;
}
interface HotelFormDocument extends System.HotelForm, mongoose.Document {
  _id: string;
}

interface CateringRequestDocument
  extends System.CateringRequest,
    mongoose.Document {
  _id: string;
}
interface SafetyTestFormDocument
  extends System.SafetyTestForm,
    mongoose.Document {
  _id: string;
}
interface CounterDocument extends System.Counter, mongoose.Document {
  _id: string;
}
interface ConfigDocument extends System.DBConfig, mongoose.Document {}
export interface SupervisorDocument
  extends System.Supervisor,
    mongoose.Document {}

export interface ProjectDocument extends System.Project, mongoose.Document {}

export interface ScheduledEventDocument
  extends System.ScheduledEvent,
    mongoose.Document {}

export interface ListDocument extends System.List, mongoose.Document {}

interface SimpleFormDocument extends System.SimpleForm, mongoose.Document {}

interface LDAPPersonDocument extends System.LDAPPerson, mongoose.Document {
  _id: string;
}

// MONGOOSE SCHEMAS
export const counterSchema = new mongoose.Schema({
  _id: String,
  sequenceValue: Number,
});

export const configSchema = new mongoose.Schema({
  ldapCacheTimestamp: Date,
});

export const supervisorSchema = new mongoose.Schema({
  name: String,
  email: String,
  form: String,
  projects: { type: [String], default: [] },
});

export const projectSchema = new mongoose.Schema({
  project: String,
  name: String,
  form: String,
  category: String,
});

export const ScheduledEventSchema = new mongoose.Schema({
  type: String,
  executionTime: Date,
  payload: Schema.Types.Mixed,
});

export const listSchema = new mongoose.Schema({
  items: {
    type: [
      {
        label: String,
        value: String,
      },
    ],
    default: [],
  },
  defaultValue: String,
  form: String,
  name: String,
  description: String,
});

export const LDAPPersonSchema = new mongoose.Schema({
  fullName: String,
  firstName: String,
  title: String,
  office: String,
  phone: String,
  memberOf: [String],
  username: String,
  email: String,
  jpeg: String,
  DN: String,
});

export const simpleFormSchema = new mongoose.Schema({
  emailSubject: String,
  emailRecipient: String,
  formType: String,
  data: Schema.Types.Mixed,
});

export const purchaseFormSchema = new mongoose.Schema({
  project: String,
  amount: Number,
  comment: String,
  product: String,
  description: String,
  frameworkAgrmt: Boolean,
  supplierName: String,
  bidder1: String,
  bidder2: String,
  bidder3: String,
  environment: String,
  motivation: String,
});
purchaseFormSchema.add(getGenericFormSchema(PURCHASE));
purchaseFormSchema.index({ "comments.msg": "text" });

export const travelFormSchema = new mongoose.Schema({
  destination: String,
  project: String,
  travelPurpose: String,
  startDate: Date,
  endDate: Date,
  expenses: {},
  totalExpenses: Number,
  activityNumber: Number,
});
travelFormSchema.add(getGenericFormSchema(TRAVEL));
travelFormSchema.index({ "comments.msg": "text" });

export const employmentRequestSchema = new mongoose.Schema({
  position: String,
  group: String,
  team: String,
  employmentType: String,
  extent: Number,
  estStartDate: Date,
  estEndDate: Date,
  estMonthlySalary: Number,
  contactedOfficeCoordinator: Boolean,
  rejectionAction: String,
  budgetType: String,
  budgetTypeDetail: String,
  budgetTypeDetail2: String,
  externallyFundedProject: String,
  externallyFundedPercentage: Number,
  primulaGroup: String,
});
employmentRequestSchema.add(getGenericFormSchema(EMPLOYMENT));
employmentRequestSchema.index({ "comments.msg": "text" });

export const GrantInitiativeSchema = new mongoose.Schema({
  responsible: String,
  coordinator: String,
  otherParties: String,
  collaborators: String,
  fundingAgency: String,
  projectName: String,
  projectDescription: String,
  beamlineAccessType: String,
  officeSpaceCount: Number,
  pooledResource: String,
  ownership: String,
  otherResources: String,
  strategyAlignment: String,
  startingYear: Number,
  people: {
    type: [
      {
        firstName: String,
        lastName: String,
        inKind: Boolean,
        years: {
          type: [{ salaryPercentage: Number, nbrMonths: Number }],
          default: [],
        },
      },
    ],
    default: [],
  },
  funding: {
    type: {
      salaries: [Number],
      investments: [Number],
      travel: [Number],
      others: [Number],
      indirectCost: [Number],
    },
    default: {
      salaries: [],
      investments: [],
      travel: [],
      others: [],
      indirectCost: [],
    },
  },
  cofunding: {
    type: {
      salaries: [Number],
      others: [Number],
      indirectCost: [Number],
    },
    default: {
      salaries: [],
      others: [],
      indirectCost: [],
    },
  },
});
GrantInitiativeSchema.add(getGenericFormSchema(GRANT_INITIATIVE));
GrantInitiativeSchema.index({ "comments.msg": "text" });

export const additionalFundingSchema = new mongoose.Schema({
  responsibleDirector: String,
  applicant: String,
  organizationalUnitType: String,
  organizationalUnitName: String,
  totalAmountRequested: Number,
  investmentDescription: String,
  investmentBackground: String,
  contingencyAmountRemaining: Number,
  totalAmountApproved: Number,
  fundingSource: String,
});
additionalFundingSchema.add(getGenericFormSchema(ADDITIONAL_FUNDING));
additionalFundingSchema.index({ "comments.msg": "text" });

const RiskEquipment = {
  mitigations: String,
  checks: [String],
};
const ExperimentDetail = {
  details: String,
  risks: String,
  mitigation: String,
  emergencyAction: String,
};

const ChemicalSample = {
  name: String,
  cas: String,
  quantity: String,
  aggregationState: String,
  safetyClassifications: [String],
};
const NanoSample = {
  name: String,
  source: String,
  sourceDetail: String,
  aggregationState: String,
  quantity: String,
  sizeDistribution: String,
  length: String,
  density: String,
  class: Number,
};

const BioSample = {
  name: String,
  category: String,
  source: String,
  sourceDetail: String,
  aggregationState: String,
  quantity: String,
};
export const riskAssessmentSchema = new mongoose.Schema({
  proposalId: String,
  title: String,
  safetyClassification: String,
  hasDates: Boolean,
  startDate: Date,
  endDate: Date,
  beamline: String,
  contactName: String,
  contactPhone: String,
  contactEmail: String,
  proposerName: String,
  proposerEmail: String,
  proposerPhone: String,
  PIName: String,
  PIEmail: String,
  PIPhone: String,
  companyName: String,
  significantModifications: String,
  sampleRemoved: String,
  labAccessRequired: Boolean,
  customBuiltEquipmentDescription: String,
  additionalChemicalProperties: String,
  additionalNanoProperties: String,
  equipments: {
    vessles: RiskEquipment,
    heater: RiskEquipment,
    cryostat: RiskEquipment,
    magnetic: RiskEquipment,
    electrochemical: RiskEquipment,
    laser: RiskEquipment,
    lamps: RiskEquipment,
    sonic: RiskEquipment,
    blowtorch: RiskEquipment,
    ribbon: RiskEquipment,
  },
  chemicalSamples: { type: [ChemicalSample], default: [] },
  nanoSamples: { type: [NanoSample], default: [] },
  bioSamples: { type: [BioSample], default: [] },
  experimentDetails: {
    lab: ExperimentDetail,
    beamline: ExperimentDetail,
    other: ExperimentDetail,
  },
  hasSignificantModifications: Boolean,
  hasHazardousEquipment: Boolean,
  hasOwnEquipment: Boolean,
  hasChemicalSamples: Boolean,
  hasNanoSamples: Boolean,
  hasBioSamples: Boolean,
  hasCMRCompound: Boolean,
});
riskAssessmentSchema.add(getGenericFormSchema(RISK_ASSESSMENT));
riskAssessmentSchema.index({ "comments.msg": "text" });

export const cateringRequestSchema = new mongoose.Schema({
  deliveryPoint: String,
  deliveryDate: Date,
  nameOfTheMeeting: String, // No longer used, expect for old forms
  purpose: String,
  activity: String,
  otherRequests: String,
  participants: String,
  orders: [{ deliveryTime: Date, servingType: String, quantity: Number }],
});
cateringRequestSchema.add(getGenericFormSchema(CATERING));
cateringRequestSchema.index({ "comments.msg": "text" });

export const hotelFormSchema = new mongoose.Schema({
  submitterPhoneNumber: String,
  guestPhoneNumber: String,
  guestName: String,
  guestEmail: String,
  checkInDate: Date,
  checkOutDate: Date,
  lateArrival: Boolean,
  breakfast: Boolean,
  paymentType: String,
  activityNumber: String,
  additionalMsg: String,
});
hotelFormSchema.add(getGenericFormSchema(HOTEL));
hotelFormSchema.index({ "comments.msg": "text" });

export const safetyTestFormSchema = new mongoose.Schema({
  employer: String,
  contactPerson: String,
  moreThanTwoMonths: Boolean,
  phoneNumber: String,
  signature: Boolean,
  answers: [{ category: String, qId: String, aId: String }],
  hasRadiation: Boolean,
  hasChemical: Boolean,
  hasBio: Boolean,
});
safetyTestFormSchema.add(getGenericFormSchema(SAFETY_TEST));
safetyTestFormSchema.index({ "comments.msg": "text" });

// MODELS
export const Counter = mongoose.model<CounterDocument>(
  "Counter",
  counterSchema
);
export const Config = mongoose.model<ConfigDocument>("Config", configSchema);
export const Supervisor = mongoose.model<SupervisorDocument>(
  "Supervisor",
  supervisorSchema
);
export const Project = mongoose.model<ProjectDocument>(
  "Project",
  projectSchema
);

export const ScheduledEvent = mongoose.model<ScheduledEventDocument>(
  "ScheduledEvent",
  ScheduledEventSchema
);
export const List = mongoose.model<ListDocument>("List", listSchema);
export const SimpleForm = mongoose.model<SimpleFormDocument>(
  "SimpleForm",
  simpleFormSchema
);
export const LDAPPerson = mongoose.model<LDAPPersonDocument>(
  "LDAPPerson",
  LDAPPersonSchema
);
purchaseFormSchema.plugin(mongoosePaginate);
export const PurchaseForm = mongoose.model<PurchaseFormDocument>(
  "PurchaseForm",
  purchaseFormSchema
);
travelFormSchema.plugin(mongoosePaginate);
export const TravelForm = mongoose.model<TravelFormDocument>(
  "TravelForm",
  travelFormSchema
);

employmentRequestSchema.plugin(mongoosePaginate);
export const EmploymentRequest = mongoose.model<EmploymentRequestDocument>(
  "EmploymentRequest",
  employmentRequestSchema
);

hotelFormSchema.plugin(mongoosePaginate);
export const HotelForm = mongoose.model<HotelFormDocument>(
  "HotelForm",
  hotelFormSchema
);

cateringRequestSchema.plugin(mongoosePaginate);
export const CateringRequest = mongoose.model<CateringRequestDocument>(
  "CateringRequest",
  cateringRequestSchema
);

safetyTestFormSchema.plugin(mongoosePaginate);
export const SafetyTestForm = mongoose.model<SafetyTestFormDocument>(
  "SafetyTestForm",
  safetyTestFormSchema
);

GrantInitiativeSchema.plugin(mongoosePaginate);
export const GrantInitiativeForm = mongoose.model<GrantInitiativeDocument>(
  "GrantInitiativeForm",
  GrantInitiativeSchema
);

additionalFundingSchema.plugin(mongoosePaginate);
export const AdditionalFundingForm = mongoose.model<AdditionalFundingDocument>(
  "AdditionalFundingForm",
  additionalFundingSchema
);

riskAssessmentSchema.plugin(mongoosePaginate);
export const RiskAssessment = mongoose.model<RiskAssessmentDocument>(
  "RiskAssessment",
  riskAssessmentSchema
);
