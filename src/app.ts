import cookieParser from "cookie-parser";
import express from "express";
import morgan from "morgan";
import cron from "node-cron";
import path from "path";
import { dumpCollections } from "./index";
import {
  clearCounter,
  clearProjectsSupervisorsAndCustomLists,
  deleteAllForms,
  deleteForms,
} from "./database/clear";
import { createPhoneBook, initMissingDefaultData } from "./defaultData";
import { cache } from "./ldap";
import { router as collectionRouter } from "./routes/collectionRoute";
import { router as emailRouter } from "./routes/emailRoute";
import { router as fileRouter } from "./routes/filesRoute";
import { router as utilRouter } from "./routes/utilRoute";
import { router as formRouter } from "./routes/formRoute";
import { router as ldapRouter } from "./routes/ldapRoute";
import { processSafetyTestReminderEvents } from "./utilities/Helpers";
import * as mailer from "./utilities/mailer";
import { auth } from "./utilities/verification";

const app = express();
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, "public")));
app.use("/", async function (req, res, next) {
  // While all request from domni frontend will have a jwt cookie, allowing auth through the body and query enabled external api calls as well
  const access = await auth(
    req.cookies[process.env.COOKIE_NAME || "maxiv-jwt-auth"] || req.body.jwt || req.query.jwt
  );
  if (!access.authenticated) {
    const err = new Error("Authentication required");
    res.statusCode = 401;
    res.statusMessage = "Authentication required";
    return next(err);
  } else {
    req.access = access;
    next();
  }
});
const rpad = (s: string, n: number) => {
  return s.length > n ? s.substring(0, n) : s.padEnd(n, " ");
};
morgan.token("verb", function (req, res) {
  try {
    if (req.method === "POST") {
      return "POST";
    }
    return `${req.method.substring(0, 3)} `;
  } catch (e) {
    return "    ";
  }
});
morgan.token("user", function (req, res) {
  try {
    return `${rpad(
      (req as any).access.name,
      20
    )} [${(req as any).access.username}]`;
  } catch (e) {
    return "         -          [unknown]";
  }
});
app.use(
  morgan(":date[iso] :user :verb :url [:status]", {
    skip: (req, res) => {
      // Skip GET on files (both profile pictures, addressbook and attachments), and also on loading supers, projects and lists:
      if (
        req.method === "GET" &&
        (req.originalUrl.startsWith("/api/files/") ||
          req.originalUrl.startsWith("/api/collection/") ||
          req.originalUrl.startsWith("/api/forms/supervisorCases")
          )
      ) {
        return true;
      }
      return false;
    },
  })
);
app.use("/api/ldap", ldapRouter);
app.use("/api/collection", collectionRouter);
app.use("/api/files", fileRouter);
app.use("/api/forms", formRouter);
app.use("/api/email", emailRouter);
app.use("/api/util", utilRouter);
// TEST: Deletes all forms, supervisors and projects for the specified form type, or all if no form type is specified. Reseeds db with default supervisor and project data. Only available to the cypress test database
app.use("/api/seedDb", async (req, res) => {
  const formType: System.FormType | undefined = req.body.formType;
  const includeForms: boolean = req.body.includeForms;
  const includeCollections: boolean = req.body.includeCollections;
  if (process.env.MONGO.includes("domni-cypress")) {
    if (includeForms) {
      if (formType) {
        await deleteForms(formType);
      } else {
        await deleteAllForms();
      }
      await clearCounter(formType);
    }
    if (includeCollections) {
      await clearProjectsSupervisorsAndCustomLists(formType);
    }

    await initMissingDefaultData();
    res.sendStatus(200);
  } else {
    return res
      .status(403)
      .send("db seeding can only be done in the cypress database");
  }
});
app.get("/api", (req, res) => {
  return res
    .status(200)
    .send(
      "No API documentation currently available. Integration possibilities are briefly addressed at https://domni-test.maxiv.lu.se/docs. Contact IMS for further help."
    );
});

// catch 404 and forward to error handler
app.use((req, res, next) => {
  return res.status(404).send("Error 404");
});

// Backup
cron.schedule("0 0 0 1 * *", () => {
  try {
    dumpCollections();
  } catch (e) {
    console.log("Error creating backup: ", e);
  }
});

// Safety test reminders
cron.schedule("0 0 06 * * *", () => {
  try {
    console.log("Processing safety test reminder events:");
    processSafetyTestReminderEvents();
  } catch (error) {
    console.log("Failed sending safety test reminder emails");
    console.error(error);
  }
});
// Ldap people and phonebook
cron.schedule("0 0 6,12 * * *", () => {
  try {
    cache(() => {
      console.log("cache updated, regenerating phonebook..");
      createPhoneBook();
    });
  } catch (error) {
    console.log("Failed reloading LDAP cache and creating phonebook", error);
  }
});
// Supervisor reminders
cron.schedule("0 0 11 */3 * *", () => {
  try {
    console.log("Processing supervisor reminders:");
    mailer.supervisorReminder();
  } catch (error) {
    console.log("Failed sending reminder emails: ", error);
  }
});
export default app;
