import { getFormTypes } from "./config";
import * as fs from "fs";
import vCard from "vcards-js";
import {
  ADDITIONAL_FUNDING,
  CATERING,
  EMPLOYMENT,
  GRANT_INITIATIVE,
  HOTEL,
  PURCHASE,
  RISK_ASSESSMENT,
  SAFETY_TEST,
  TRAVEL,
} from "./constants";
import { clearLDAPPeople } from "./database/clear";
import { addCollection, getCollection } from "./database/collection";
import * as counter from "./database/counters";
import { cache } from "./ldap";
import { Config, LDAPPerson, Project, Supervisor } from "./models/mongoSchema";

export const initMissingDefaultData = async () => {
  counter.initialize();
  getFormTypes().forEach(async (form) => {
    const projects = await getCollection(Project, form);
    if (projects.length === 0) {
      console.log("No projects for '" + form + "'. Creating default..");
      defaultData.forms[form].projects.forEach((project: System.Project) => {
        addCollection(Project, project);
      });
    }
    const supers = await getCollection(Supervisor, form);
    if (supers.length === 0) {
      console.log("No supervisors for '" + form + "'. Creating default..");
      defaultData.forms[form].supervisors.forEach(
        (supervisor: System.Supervisor) => {
          addCollection(Supervisor, supervisor);
        }
      );
    }
  });
};
const getLastName = (fullName: string) => {
  const names = fullName.split(" ");
  if (names.length === 0) {
    // can this really happen?
    return "";
  }
  if (names.length === 1) {
    return fullName;
  }
  return names[names.length - 1];
};
export const createPhoneBook = async () => {
  const people = await LDAPPerson.find({});
  let phoneBook = "";
  people.forEach((person) => {
    const card = vCard();
    card.firstName = person.firstName;
    card.lastName = getLastName(person.fullName);
    card.organization = "MAX IV";
    card.workPhone = person.phone;
    card.workEmail = person.email;
    card.title = person.title;
    phoneBook += card.getFormattedString();
  });

  fs.writeFile("./files/phoneBook.vcf", phoneBook, (e) => {
    if (e) {
      console.log("error saving phoneBook.vcf", e);
    }
  });
};

const defaultData: System.DefaultData = {
  forms: {
    CATERING_REQUEST: {
      projects: [{ project: "Default", category: "Default", form: CATERING }],
      supervisors: [
        {
          name: "MAX IV Reception",
          email: "reception@maxiv.lu.se",
          form: CATERING,
          projects: ["Default"],
        },
      ],
    },
    HOTEL_BOOKING: {
      projects: [{ project: "Default", category: "Default", form: HOTEL }],
      supervisors: [
        {
          name: "MAX IV Reception",
          email: "reception@maxiv.lu.se",
          form: HOTEL,
          projects: ["Default"],
        },
      ],
    },
    SAFETY_TEST: {
      projects: [
        { project: "Default", category: "Default", form: SAFETY_TEST },
      ],
      supervisors: [
        {
          name: "MAX IV Reception",
          email: "reception@maxiv.lu.se",
          form: SAFETY_TEST,
          projects: ["Default"],
        },
      ],
    },
    GRANT_INITIATIVE: {
      projects: [
        { project: "Default", category: "Default", form: GRANT_INITIATIVE },
      ],
      supervisors: [
        {
          name: "Ian McNulty",
          email: "ian.mcnulty@maxiv.lu.se",
          form: GRANT_INITIATIVE,
          projects: [],
        },
        {
          name: "Pedro Fernandes Tavares",
          email: "pedro.fernandes_tavares@maxiv.lu.se",
          form: GRANT_INITIATIVE,
          projects: [],
        },
        {
          name: "Conny Såthe",
          email: "conny.sathe@maxiv.lu.se",
          form: GRANT_INITIATIVE,
          projects: [],
        },
        {
          name: "Marjolein Thunnissen",
          email: "marjolein.thunnissen@maxiv.lu.se",
          form: GRANT_INITIATIVE,
          projects: [],
        },
        {
          name: "Lena Qvist",
          email: "lena.qvist@maxiv.lu.se",
          form: GRANT_INITIATIVE,
          projects: [],
        },
        {
          name: "Marianne Helgertz",
          email: "marianne.helgertz@maxiv.lu.se",
          form: GRANT_INITIATIVE,
          projects: [],
        },
      ],
    },
    ADDITIONAL_FUNDING: {
      projects: [
        { project: "Default", category: "Default", form: ADDITIONAL_FUNDING },
      ],
      supervisors: [
        {
          name: "Ian McNulty",
          email: "ian.mcnulty@maxiv.lu.se",
          form: ADDITIONAL_FUNDING,
          projects: [],
        },
        {
          name: "Pedro Fernandes Tavares",
          email: "pedro.fernandes_tavares@maxiv.lu.se",
          form: ADDITIONAL_FUNDING,
          projects: [],
        },
        {
          name: "Conny Såthe",
          email: "conny.sathe@maxiv.lu.se",
          form: ADDITIONAL_FUNDING,
          projects: [],
        },
        {
          name: "Marjolein Thunnissen",
          email: "marjolein.thunnissen@maxiv.lu.se",
          form: ADDITIONAL_FUNDING,
          projects: [],
        },
        {
          name: "Lena Qvist",
          email: "lena.qvist@maxiv.lu.se",
          form: ADDITIONAL_FUNDING,
          projects: [],
        },
        {
          name: "Marianne Helgertz",
          email: "marianne.helgertz@maxiv.lu.se",
          form: ADDITIONAL_FUNDING,
          projects: [],
        },
      ],
    },
    EMPLOYMENT_REQUEST: {
      projects: [
        {
          project: "PROJ SSF Zakharov 125364",
          category: "Default",
          form: EMPLOYMENT,
        },
        {
          project: "PROJ VR RÅC GU Plivelic 126850",
          category: "Default",
          form: EMPLOYMENT,
        },
        {
          project: "PROJ C40 Balder proj 1 128072",
          category: "Default",
          form: EMPLOYMENT,
        },
        {
          project: "PROJ C40 Koherens proj.2 128617",
          category: "Default",
          form: EMPLOYMENT,
        },
        {
          project: "PROJ C40 Nanoindenter proj.3 128618",
          category: "Default",
          form: EMPLOYMENT,
        },
        {
          project: "PROJ CALIPSOplus Mgt 127908",
          category: "Default",
          form: EMPLOYMENT,
        },
        {
          project: "PROJ EU CALIPSOplus NA1 128968",
          category: "Default",
          form: EMPLOYMENT,
        },
        {
          project: "PROJ EU CALIPSOplus NA2 128970",
          category: "Default",
          form: EMPLOYMENT,
        },
        {
          project: "PROJ EU CALIPSOplus NA3 128971",
          category: "Default",
          form: EMPLOYMENT,
        },
        {
          project: "PROJ EU CALIPSOplus NA4 128972",
          category: "Default",
          form: EMPLOYMENT,
        },
        {
          project: "PROJ EU CALIPSOplus JRA1 136117",
          category: "Default",
          form: EMPLOYMENT,
        },
        {
          project: "PROJ EU CALIPSOplus TamaTA 128973",
          category: "Default",
          form: EMPLOYMENT,
        },
        {
          project: "PROJ EU Mummering Mokso 131560",
          category: "Default",
          form: EMPLOYMENT,
        },
        {
          project: "PROJ VR FragMAX Lima 134341",
          category: "Default",
          form: EMPLOYMENT,
        },
        {
          project: "PROJ SSF AdaptoCell Sigfridsso 134374",
          category: "Default",
          form: EMPLOYMENT,
        },
        {
          project: "PROJ VR NanoSPAM K.Thånell 134375",
          category: "Default",
          form: EMPLOYMENT,
        },
        {
          project: "PROJ SSF In-form Just 136538",
          category: "Default",
          form: EMPLOYMENT,
        },
        {
          project: "PROJ EU ExPaNDS D.Spruce 138027",
          category: "Default",
          form: EMPLOYMENT,
        },
        {
          project: "PROJ Vin.NextBioForm 2 Terry 138463",
          category: "Default",
          form: EMPLOYMENT,
        },
        {
          project: "PROJ iNEXT Discovery 139525",
          category: "Default",
          form: EMPLOYMENT,
        },
        {
          project: "PROJ Formas NLoF S.Maric 140154",
          category: "Default",
          form: EMPLOYMENT,
        },
        {
          project: "PROJ RÅC KTH U.Johansson 141207",
          category: "Default",
          form: EMPLOYMENT,
        },
        {
          project: "PROJ Vin.Ultrapure water Terry 141317",
          category: "Default",
          form: EMPLOYMENT,
        },
        {
          project: "PROJ RÅC LU A.Terry 141330",
          category: "Default",
          form: EMPLOYMENT,
        },
        {
          project: "PROJ VR E.Mårsell 141372",
          category: "Default",
          form: EMPLOYMENT,
        },
      ],
      supervisors: [
        {
          name: "Ian McNulty",
          email: "ian.mcnulty@maxiv.lu.se",
          form: EMPLOYMENT,
          projects: [],
        },
        {
          name: "Pedro Fernandes Tavares",
          email: "pedro.fernandes_tavares@maxiv.lu.se",
          form: EMPLOYMENT,
          projects: [],
        },
        {
          name: "Conny Såthe",
          email: "conny.sathe@maxiv.lu.se",
          form: EMPLOYMENT,
          projects: [],
        },
        {
          name: "Marjolein Thunnissen",
          email: "marjolein.thunnissen@maxiv.lu.se",
          form: EMPLOYMENT,
          projects: [],
        },
        {
          name: "Lena Qvist",
          email: "lena.qvist@maxiv.lu.se",
          form: EMPLOYMENT,
          projects: [],
        },
        {
          name: "Marianne Helgertz",
          email: "marianne.helgertz@maxiv.lu.se",
          form: EMPLOYMENT,
          projects: [],
        },
      ],
    },
    TRAVEL: {
      projects: [
        {
          project: "MicroMAX 133056",
          form: TRAVEL,
          category: "Beamline project (839533)",
        },
        {
          project: "ForMAX 131011",
          form: TRAVEL,
          category: "Beamline project (839533)",
        },
        {
          project: "PROJ SSF Zakharov 125364",
          form: TRAVEL,
          category: "Other external project",
        },
        {
          project: "PROJ VR RÅC GU Plivelic 126850",
          form: TRAVEL,
          category: "Other external project",
        },
        {
          project: "PROJ C40 Balder proj 1 128072",
          form: TRAVEL,
          category: "Other external project",
        },
        {
          project: "PROJ C40 Koherens proj.2 128617",
          form: TRAVEL,
          category: "Other external project",
        },
        {
          project: "PROJ C40 Nanoindenter proj.3 128618",
          form: TRAVEL,
          category: "Other external project",
        },
        {
          project: "PROJ CALIPSOplus Mgt 127908",
          form: TRAVEL,
          category: "Other external project",
        },
        {
          project: "PROJ EU CALIPSOplus NA1 128968",
          form: TRAVEL,
          category: "Other external project",
        },
        {
          project: "PROJ EU CALIPSOplus NA2 128970",
          form: TRAVEL,
          category: "Other external project",
        },
        {
          project: "PROJ EU CALIPSOplus NA3 128971",
          form: TRAVEL,
          category: "Other external project",
        },
        {
          project: "PROJ EU CALIPSOplus NA4 128972",
          form: TRAVEL,
          category: "Other external project",
        },
        {
          project: "PROJ EU CALIPSOplus JRA1 136117",
          form: TRAVEL,
          category: "Other external project",
        },
        {
          project: "PROJ EU CALIPSOplus TamaTA 128973",
          form: TRAVEL,
          category: "Other external project",
        },
        {
          project: "PROJ EU Mummering Mokso 131560",
          form: TRAVEL,
          category: "Other external project",
        },
        {
          project: "PROJ VR FragMAX Lima 134341",
          form: TRAVEL,
          category: "Other external project",
        },
        {
          project: "PROJ SSF AdaptoCell Sigfridsso 134374",
          form: TRAVEL,
          category: "Other external project",
        },
        {
          project: "PROJ VR NanoSPAM K.Thånell 134375",
          form: TRAVEL,
          category: "Other external project",
        },
        {
          project: "PROJ SSF In-form Just 136538",
          form: TRAVEL,
          category: "Other external project",
        },
        {
          project: "PROJ EU ExPaNDS D.Spruce 138027",
          form: TRAVEL,
          category: "Other external project",
        },
        {
          project: "PROJ Vin.NextBioForm 2 Terry 138463",
          form: TRAVEL,
          category: "Other external project",
        },
        {
          project: "PROJ iNEXT Discovery 139525",
          form: TRAVEL,
          category: "Other external project",
        },
        {
          project: "PROJ Formas NLoF S.Maric 140154",
          form: TRAVEL,
          category: "Other external project",
        },
        {
          project: "PROJ RÅC KTH U.Johansson 141207",
          form: TRAVEL,
          category: "Other external project",
        },
        {
          project: "PROJ Vin.Ultrapure water Terry 141317",
          form: TRAVEL,
          category: "Other external project",
        },
        {
          project: "PROJ RÅC LU A.Terry 141330",
          form: TRAVEL,
          category: "Other external project",
        },
        {
          project: "PROJ VR E.Mårsell 141372",
          form: TRAVEL,
          category: "Other external project",
        },
        {
          project: "Accelerator dev. ADM OH 130558",
          form: TRAVEL,
          category: "Operation budget (839530)",
        },
        {
          project: "Accelerator dev. ADM DIR  139580",
          form: TRAVEL,
          category: "Operation budget (839530)",
        },
        {
          project: "Adminstrative ADM DIR  139579",
          form: TRAVEL,
          category: "Operation budget (839530)",
        },
        {
          project: "AFSG Travel and conf. 114678",
          form: TRAVEL,
          category: "Operation budget (839530)",
        },
        {
          project: "AFSG SXL  Travel and conf. 133840",
          form: TRAVEL,
          category: "Operation budget (839530)",
        },
        {
          project: "Balder Travel and conf. 119724",
          form: TRAVEL,
          category: "Operation budget (839530)",
        },
        {
          project: "BioMAX Travel and conf. 119741",
          form: TRAVEL,
          category: "Operation budget (839530)",
        },
        {
          project: "Bloch Travel and conf. 119702",
          form: TRAVEL,
          category: "Operation budget (839530)",
        },
        {
          project: "Beamline Office ADM OH 130661",
          form: TRAVEL,
          category: "Operation budget (839530)",
        },
        {
          project: "Communication ADM OH 130561",
          form: TRAVEL,
          category: "Operation budget (839530)",
        },
        {
          project: "Controls & IT ADM OH 130651",
          form: TRAVEL,
          category: "Operation budget (839530)",
        },
        {
          project: "CoSAXS Travel and conf. 119726",
          form: TRAVEL,
          category: "Operation budget (839530)",
        },
        {
          project: "CPO ADM OH 140510",
          form: TRAVEL,
          category: "Operation budget (839530)",
        },
        {
          project: "DanMAX  Travel and conf. 119728",
          form: TRAVEL,
          category: "Operation budget (839530)",
        },
        {
          project: "Diffraction & Scattering ADM OH 130649",
          form: TRAVEL,
          category: "Operation budget (839530)",
        },
        {
          project: "Engineering l ADM OH 136931",
          form: TRAVEL,
          category: "Operation budget (839530)",
        },
        {
          project: "Engineering ll ADM OH 130554",
          form: TRAVEL,
          category: "Operation budget (839530)",
        },
        {
          project: "FemtoMAX Travel and conf. 119721",
          form: TRAVEL,
          category: "Operation budget (839530)",
        },
        {
          project: "FinEstBeaMS Travel and conf. 119705",
          form: TRAVEL,
          category: "Operation budget (839530)",
        },
        {
          project: "FIOS ADM OH 113636",
          form: TRAVEL,
          category: "Operation budget (839530)",
        },
        {
          project: "FlexPES Travel and conf. 119713",
          form: TRAVEL,
          category: "Operation budget (839530)",
        },
        {
          project: "ForMAX Travel and conf. 135535",
          form: TRAVEL,
          category: "Operation budget (839530)",
        },
        {
          project: "Head office ADM OH 113405",
          form: TRAVEL,
          category: "Operation budget (839530)",
        },
        {
          project: "Hippie Travel and conf. 119699",
          form: TRAVEL,
          category: "Operation budget (839530)",
        },
        {
          project: "HR ADM OH 113666",
          form: TRAVEL,
          category: "Operation budget (839530)",
        },
        {
          project: "ILO Industrial Outreach & Training 125274",
          form: TRAVEL,
          category: "Operation budget (839530)",
        },
        {
          project: "Imaging ADM OH 130648",
          form: TRAVEL,
          category: "Operation budget (839530)",
        },
        {
          project: "ILO ADM OH  129470",
          form: TRAVEL,
          category: "Operation budget (839530)",
        },
        {
          project: "Insertion Devices ADM OH 130662",
          form: TRAVEL,
          category: "Operation budget (839530)",
        },
        {
          project: "IT Control System Hardware ADM OH 136914",
          form: TRAVEL,
          category: "Operation budget (839530)",
        },
        {
          project: "IT Control System Software ADM OH 136915",
          form: TRAVEL,
          category: "Operation budget (839530)",
        },
        {
          project: "IT Infrastructure ADM OH 136917",
          form: TRAVEL,
          category: "Operation budget (839530)",
        },
        {
          project: "IT Scientific and information ADM OH 136916",
          form: TRAVEL,
          category: "Operation budget (839530)",
        },
        {
          project: "LDM Travel and conf. 134842",
          form: TRAVEL,
          category: "Operation budget (839530)",
        },
        {
          project: "Life Science  ADM DIR  139582",
          form: TRAVEL,
          category: "Operation budget (839530)",
        },
        {
          project: "MaxPEEM Travel and conf. 119715",
          form: TRAVEL,
          category: "Operation budget (839530)",
        },
        {
          project: "MedMAX Travel and conf. 119734",
          form: TRAVEL,
          category: "Operation budget (839530)",
        },
        {
          project: "MX ADM OH 130867",
          form: TRAVEL,
          category: "Operation budget (839530)",
        },
        {
          project: "MicroMAX Travel and conf. 136358",
          form: TRAVEL,
          category: "Operation budget (839530)",
        },
        {
          project: "NanoMAX Travel and conf. 119718",
          form: TRAVEL,
          category: "Operation budget (839530)",
        },
        {
          project: "Operations ADM OH 130556",
          form: TRAVEL,
          category: "Operation budget (839530)",
        },
        {
          project: "Physical Dir ADM DIR  139581",
          form: TRAVEL,
          category: "Operation budget (839530)",
        },
        {
          project: "Research coordinator ADM OH 119483",
          form: TRAVEL,
          category: "Operation budget (839530)",
        },
        {
          project: "Radio Frequency  ADM OH 130559",
          form: TRAVEL,
          category: "Operation budget (839530)",
        },
        {
          project: "Safety ADM OH 130568",
          form: TRAVEL,
          category: "Operation budget (839530)",
        },
        {
          project: "SoftiMAX Travel and conf. 119708",
          form: TRAVEL,
          category: "Operation budget (839530)",
        },
        {
          project: "Species Travel and conf. 119712",
          form: TRAVEL,
          category: "Operation budget (839530)",
        },
        {
          project: "Spectroscopy ADM OH 130653",
          form: TRAVEL,
          category: "Operation budget (839530)",
        },
        {
          project: "STM Travel and conf. 125004",
          form: TRAVEL,
          category: "Operation budget (839530)",
        },
        {
          project: "User Office Travel and conf. 130652",
          form: TRAVEL,
          category: "Operation budget (839530)",
        },
        {
          project: "Veritas Travel and conf. 119696",
          form: TRAVEL,
          category: "Operation budget (839530)",
        },
      ],
      supervisors: [
        {
          name: "Alexei Preobrajenski",
          email: "alexei.preobrajenski@maxiv.lu.se",
          projects: [],
          form: TRAVEL,
        },
        {
          name: "Ann Terry",
          email: "ann.terry@maxiv.lu.se",
          projects: [
            "PROJ Vin.NextBioForm 2 Terry 138463",
            "PROJ Vin.Ultrapure water Terry 141317",
            "PROJ RÅC LU A.Terry 141330",
            "Diffraction & Scattering ADM OH 130649",
          ],
          form: TRAVEL,
        },
        {
          name: "Balasubramanian Thiagarajan",
          email: "balasubramanian.thiagarajan@maxiv.lu.se",
          projects: [],
          form: TRAVEL,
        },
        {
          name: "Conny Sathe",
          email: "conny.sathe@maxiv.lu.se",
          projects: [],
          form: TRAVEL,
        },
        {
          name: "Darren Spruce",
          email: "darren.spruce@maxiv.lu.se",
          projects: [
            "PROJ EU ExPaNDS D.Spruce 138027",
            "Controls & IT ADM OH 130651",
          ],
          form: TRAVEL,
        },

        {
          name: "Eshraq Al_dmour",
          email: "eshraq.al_dmour@maxiv.lu.se",
          projects: [],
          form: TRAVEL,
        },

        {
          name: "Franz Hennies",
          email: "franz.hennies@maxiv.lu.se",
          projects: [
            "PROJ C40 Balder proj 1 128072",
            "PROJ C40 Koherens proj.2 128617",
            "PROJ C40 Nanoindenter proj.3 128618",
            "PROJ CALIPSOplus Mgt 127908",
            "PROJ EU CALIPSOplus NA1 128968",
            "PROJ EU CALIPSOplus NA2 128970",
            "PROJ EU CALIPSOplus NA4 128972",
            "PROJ EU CALIPSOplus JRA1 136117",
            "User Office Travel and conf. 130652",
          ],
          form: TRAVEL,
        },

        {
          name: "Hamed Tarawneh",
          email: "hamed.tarawneh@maxiv.lu.se",
          projects: [],
          form: TRAVEL,
        },

        {
          name: "Ian Mcnulty",
          email: "ian.mcnulty@maxiv.lu.se",
          projects: [],
          form: TRAVEL,
        },

        {
          name: "Jan Knudsen",
          email: "jan.knudsen@maxiv.lu.se",
          projects: [],
          form: TRAVEL,
        },

        {
          name: "Jorgen Larsson",
          email: "jorgen.larsson@maxiv.lu.se",
          projects: [],
          form: TRAVEL,
        },
        {
          name: "Karina Thanell",
          email: "karina.thanell@maxiv.lu.se",
          projects: [],
          form: TRAVEL,
        },
        {
          name: "Katarina Jacobsson",
          email: "katarina.jacobsson@maxiv.lu.se",
          projects: [],
          form: TRAVEL,
        },
        {
          name: "Katarina Noren",
          email: "katarina.noren@maxiv.lu.se",
          projects: [],
          form: TRAVEL,
        },
        {
          name: "Kim Nygard",
          email: "kim.nygard@maxiv.lu.se",
          projects: ["ForMAX 131011"],
          form: TRAVEL,
        },
        {
          name: "Magnus Berglund",
          email: "magnus.berglund@maxiv.lu.se",
          projects: [],
          form: TRAVEL,
        },
        {
          name: "Magnus Sjostrom",
          email: "magnus.sjostrom@maxiv.lu.se",
          projects: [],
          form: TRAVEL,
        },
        {
          name: "Marcus Agaker",
          email: "marcus.agaker@maxiv.lu.se",
          projects: [],
          form: TRAVEL,
        },
        {
          name: "Marjolein Thunnissen",
          email: "marjolein.thunnissen@maxiv.lu.se",
          projects: [
            "BioMAX Travel and conf. 119741",
            "PROJ iNEXT Discovery 139525",
            "ILO Industrial Outreach & Training 125274",
            "MedMAX Travel and conf. 119734",
            "MX ADM OH 130867",
            "ILO ADM OH 129470",
          ],
          form: TRAVEL,
        },
        {
          name: "Pedro Fernandes Taletes",
          email: "pedro.fernandes_taletes@maxiv.lu.se",
          projects: [],
          form: TRAVEL,
        },
        {
          name: "Rainer Parna",
          email: "rainer.parna@maxiv.lu.se",
          projects: [],
          form: TRAVEL,
        },
        {
          name: "Sara Thorin",
          email: "sara.thorin@maxiv.lu.se",
          projects: ["Accelerator - Linac"],
          form: TRAVEL,
        },
        {
          name: "Ulf Johansson",
          email: "ulf.johansson@maxiv.lu.se",
          projects: [],
          form: TRAVEL,
        },
        {
          name: "Thomas Ursby",
          email: "thomas.ursby@maxiv.lu.se",
          projects: ["MicroMAX 133056"],
          form: TRAVEL,
        },
        {
          name: "Tomas Plivelic",
          email: "tomas.plivelic@maxiv.lu.se",
          projects: [],
          form: TRAVEL,
        },
        {
          name: "Uwe Muller",
          email: "uwe.muller@maxiv.lu.se",
          projects: [],
          form: TRAVEL,
        },
        {
          name: "Yngve Cereniu",
          email: "yngve.cerenius@maxiv.lu.se",
          projects: [],
          form: TRAVEL,
        },
        {
          name: "Kajsa Sigfridsson Clauss",
          email: "kajsa.sigfridsson_clauss@maxiv.lu.se",
          projects: [],
          form: TRAVEL,
        },
      ],
    },
    RISK_ASSESSMENT: {
      projects: [
        { project: "Default", category: "Default", form: RISK_ASSESSMENT },
      ],
      supervisors: [
        {
          name: "EST - Experimental Safety Team",
          email: "est@maxiv.lu.se",
          form: RISK_ASSESSMENT,
          projects: ["Default"],
        },
      ],
    },
    PURCHASE_APPLICATION: {
      projects: [
        {
          project: "ARPES/BLOCH",
          form: PURCHASE,
          category: "BL PROJECTS (kst.839533)",
        },
        {
          project: "BALDER",
          form: PURCHASE,
          category: "BL PROJECTS (kst.839533)",
        },
        {
          project: "CoSAXS",
          form: PURCHASE,
          category: "BL PROJECTS (kst.839533)",
        },
        {
          project: "DanMAX",
          form: PURCHASE,
          category: "BL PROJECTS (kst.839533)",
        },
        {
          project: "FemtoMAX",
          form: PURCHASE,
          category: "BL PROJECTS (kst.839533)",
        },
        {
          project: "FinEST",
          form: PURCHASE,
          category: "BL PROJECTS (kst.839533)",
        },
        {
          project: "FlexPES",
          form: PURCHASE,
          category: "BL PROJECTS (kst.839533)",
        },
        {
          project: "ForMAX",
          form: PURCHASE,
          category: "BL PROJECTS (kst.839533)",
        },
        {
          project: "MaxPEEM",
          form: PURCHASE,
          category: "BL PROJECTS (kst.839533)",
        },
        {
          project: "MicroMAX",
          form: PURCHASE,
          category: "BL PROJECTS (kst.839533)",
        },
        {
          project: "NanoMAX",
          form: PURCHASE,
          category: "BL PROJECTS (kst.839533)",
        },
        {
          project: "SoftiMAX",
          form: PURCHASE,
          category: "BL PROJECTS (kst.839533)",
        },
        {
          project: "VERITAS",
          form: PURCHASE,
          category: "BL PROJECTS (kst.839533)",
        },
        {
          project: "DS BALDER, Operations",
          form: PURCHASE,
          category: "BL OPERATIONS (kst.839530)",
        },
        {
          project: "DS CoSAXS, Operations",
          form: PURCHASE,
          category: "BL OPERATIONS (kst.839530)",
        },
        {
          project: "DanMAX, Operations",
          form: PURCHASE,
          category: "BL OPERATIONS (kst.839530)",
        },
        {
          project: "DS FemtoMAX, Operations",
          form: PURCHASE,
          category: "BL OPERATIONS (kst.839530)",
        },
        {
          project: "IM NanoMAX, Operations",
          form: PURCHASE,
          category: "BL OPERATIONS (kst.839530)",
        },
        {
          project: "IM STM, Operations",
          form: PURCHASE,
          category: "BL OPERATIONS (kst.839530)",
        },
        {
          project: "MX BioMAX, Operations",
          form: PURCHASE,
          category: "BL OPERATIONS (kst.839530)",
        },
        {
          project: "MX MicroMAX, Operations",
          form: PURCHASE,
          category: "BL OPERATIONS (kst.839530)",
        },
        {
          project: "SP BLOCH, Operations",
          form: PURCHASE,
          category: "BL OPERATIONS (kst.839530)",
        },
        {
          project: "APRES, Operations",
          form: PURCHASE,
          category: "BL OPERATIONS (kst.839530)",
        },
        {
          project: "SP FinEST, Operations",
          form: PURCHASE,
          category: "BL OPERATIONS (kst.839530)",
        },
        {
          project: "SP FlexPES, Operations",
          form: PURCHASE,
          category: "BL OPERATIONS (kst.839530)",
        },
        {
          project: "SP HIPPIE, Operations",
          form: PURCHASE,
          category: "BL OPERATIONS (kst.839530)",
        },
        {
          project: "SP LDM, Operations",
          form: PURCHASE,
          category: "BL OPERATIONS (kst.839530)",
        },
        {
          project: "IM MaxPEEM, Operations",
          form: PURCHASE,
          category: "BL OPERATIONS (kst.839530)",
        },
        {
          project: "IM SoftiMAX, Operations",
          form: PURCHASE,
          category: "BL OPERATIONS (kst.839530)",
        },
        {
          project: "SP SPECIES, Operations",
          form: PURCHASE,
          category: "BL OPERATIONS (kst.839530)",
        },
        {
          project: "SP VERITAS, Operations",
          form: PURCHASE,
          category: "BL OPERATIONS (kst.839530)",
        },
        {
          project: "Administrative division",
          form: PURCHASE,
          category: "OTHER OPERATIONS (kst.839530)",
        },
        {
          project: "Communication",
          form: PURCHASE,
          category: "OTHER OPERATIONS (kst.839530)",
        },
        {
          project: "Engineering I",
          form: PURCHASE,
          category: "OTHER OPERATIONS (kst.839530)",
        },
        {
          project: "Engineering II",
          form: PURCHASE,
          category: "OTHER OPERATIONS (kst.839530)",
        },
        {
          project: "Accelerator - R1",
          form: PURCHASE,
          category: "OTHER OPERATIONS (kst.839530)",
        },
        {
          project: "Accelerator - R3",
          form: PURCHASE,
          category: "OTHER OPERATIONS (kst.839530)",
        },
        {
          project: "Accelerator - Linac",
          form: PURCHASE,
          category: "OTHER OPERATIONS (kst.839530)",
        },
        {
          project: "Beamline office (BO)",
          form: PURCHASE,
          category: "OTHER OPERATIONS (kst.839530)",
        },
        {
          project: "Insertion Devices",
          form: PURCHASE,
          category: "OTHER OPERATIONS (kst.839530)",
        },
        {
          project: "IT & Controls",
          form: PURCHASE,
          category: "OTHER OPERATIONS (kst.839530)",
        },
        {
          project: "Safety",
          form: PURCHASE,
          category: "OTHER OPERATIONS (kst.839530)",
        },
      ],
      supervisors: [
        {
          name: "Alexei Preobrajenski",
          email: "alexei.preobrajenski@maxiv.lu.se",
          projects: ["MaxPEEM", "FlexPES"],
          form: PURCHASE,
        },
        {
          name: "Ann Terry",
          email: "ann.terry@maxiv.lu.se",
          projects: [
            "DS BALDER, Operations",
            "DS CoSAXS, Operations",
            "DS FemtoMAX, Operations",
          ],
          form: PURCHASE,
        },
        {
          name: "Balasubramanian Thiagarajan",
          email: "balasubramanian.thiagarajan@maxiv.lu.se",
          projects: ["ARPES/BLOCH"],
          form: PURCHASE,
        },
        {
          name: "Conny Sathe",
          email: "conny.sathe@maxiv.lu.se",
          projects: [
            "APRES, Operations",
            "SP FinEST, Operations",
            "SP FlexPES, Operations",
            "SP HIPPIE, Operations",
            "SP SPECIES, Operations",
            "SP VERITAS, Operations",
            "SP BLOCH, Operations",
            "SP LDM, Operations",
          ],
          form: PURCHASE,
        },
        {
          name: "Darren Spruce",
          email: "darren.spruce@maxiv.lu.se",
          projects: ["IT & Controls"],
          form: PURCHASE,
        },

        {
          name: "Eshraq Al_dmour",
          email: "eshraq.al_dmour@maxiv.lu.se",
          projects: ["Engineering I"],
          form: PURCHASE,
        },

        {
          name: "Franz Hennies",
          email: "franz.hennies@maxiv.lu.se",
          projects: [],
          form: PURCHASE,
        },

        {
          name: "Hamed Tarawneh",
          email: "hamed.tarawneh@maxiv.lu.se",
          projects: ["Insertion Devices"],
          form: PURCHASE,
        },

        {
          name: "Ian Mcnulty",
          email: "ian.mcnulty@maxiv.lu.se",
          projects: ["Communication"],
          form: PURCHASE,
        },

        {
          name: "Jan Knudsen",
          email: "jan.knudsen@maxiv.lu.se",
          projects: [],
          form: PURCHASE,
        },

        {
          name: "Jorgen Larsson",
          email: "jorgen.larsson@maxiv.lu.se",
          projects: ["FemtoMAX"],
          form: PURCHASE,
        },
        {
          name: "Karina Thanell",
          email: "karina.thanell@maxiv.lu.se",
          projects: [
            "SoftiMAX",
            "IM NanoMAX, Operations",
            "IM MaxPEEM, Operations",
            "IM SoftiMAX, Operations",
            "IM STM, Operation",
          ],
          form: PURCHASE,
        },
        {
          name: "Katarina Jacobsson",
          email: "katarina.jacobsson@maxiv.lu.se",
          projects: ["Administrative division"],
          form: PURCHASE,
        },
        {
          name: "Katarina Noren",
          email: "katarina.noren@maxiv.lu.se",
          projects: ["Safety"],
          form: PURCHASE,
        },
        {
          name: "Kim Nygard",
          email: "kim.nygard@maxiv.lu.se",
          projects: ["ForMAX"],
          form: PURCHASE,
        },
        {
          name: "Magnus Berglund",
          email: "magnus.berglund@maxiv.lu.se",
          projects: ["Engineering II"],
          form: PURCHASE,
        },
        {
          name: "Magnus Sjostrom",
          email: "magnus.sjostrom@maxiv.lu.se",
          projects: ["Accelerator - R1", "Accelerator - R3"],
          form: PURCHASE,
        },
        {
          name: "Marcus Agaker",
          email: "marcus.agaker@maxiv.lu.se",
          projects: ["VERITAS"],
          form: PURCHASE,
        },
        {
          name: "Marjolein Thunnissen",
          email: "marjolein.thunnissen@maxiv.lu.se",
          projects: [],
          form: PURCHASE,
        },
        {
          name: "Pedro Fernandes Taletes",
          email: "pedro.fernandes_taletes@maxiv.lu.se",
          projects: [],
          form: PURCHASE,
        },
        {
          name: "Rainer Parna",
          email: "rainer.parna@maxiv.lu.se",
          projects: ["FinEST"],
          form: PURCHASE,
        },
        {
          name: "Sara Thorin",
          email: "sara.thorin@maxiv.lu.se",
          projects: ["Accelerator - Linac"],
          form: PURCHASE,
        },
        {
          name: "Ulf Johansson",
          email: "ulf.johansson@maxiv.lu.se",
          projects: ["NanoMAX"],
          form: PURCHASE,
        },
        {
          name: "Thomas Ursby",
          email: "thomas.ursby@maxiv.lu.se",
          projects: ["MicroMAX"],
          form: PURCHASE,
        },
        {
          name: "Tomas Plivelic",
          email: "tomas.plivelic@maxiv.lu.se",
          projects: ["CoSAXS"],
          form: PURCHASE,
        },
        {
          name: "Uwe Muller",
          email: "uwe.muller@maxiv.lu.se",
          projects: ["MX MicroMAX, Operations", "MX BioMAX, Operations"],
          form: PURCHASE,
        },
        {
          name: "Yngve Cereniu",
          email: "yngve.cerenius@maxiv.lu.se",
          projects: ["DanMAX", "Beamline office (BO)", "DanMAX, Operations"],
          form: PURCHASE,
        },
        {
          name: "Kajsa Sigfridsson Clauss",
          email: "kajsa.sigfridsson_clauss@maxiv.lu.se",
          projects: ["BALDER"],
          form: PURCHASE,
        },
      ],
    },
  },
};
