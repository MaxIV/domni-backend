import mongoose from "mongoose";
import { DocumentToObjectOptions } from "mongoose";
declare global {
  export namespace Express {
    interface Request {
      access?: System.Access;
    }
  }
  export namespace System {
    type EventRole = "Submitter" | "Supervisor" | "Administrator" | "System";
    type CollectionType = "LIST" | "PROJECT" | "SUPERVISOR";
    type FormType =
      | "PURCHASE_APPLICATION"
      | "TRAVEL"
      | "EMPLOYMENT_REQUEST"
      | "GRANT_INITIATIVE"
      | "ADDITIONAL_FUNDING"
      | "HOTEL_BOOKING"
      | "CATERING_REQUEST"
      | "RISK_ASSESSMENT"
      | "SAFETY_TEST";
    type FormStatus =
      | "UNSAVED"
      | "DRAFT"
      | "PENDING"
      | "APPROVED"
      | "REJECTED"
      | "DELETED";
    type FormStatusAction = FormStatus | "SUPPLANTED";
    type FormUpdateType =
      | "DRAFT_SUBMITTAL"
      | "RESUBMITTAL"
      | "EDIT"
      | "COMMENT"
      | "SUPERVISOR_REASSIGNMENT"
      | "APPROVAL"
      | "REJECTION"
      | "EXTERNAL_ID"
      | "DELETE"
      | "ATTACHMENT"
      | "ARCHIVING"
      | "POSTPONE_REMINDER";
    type EventType = "Comment" | "SupervisorAction" | "Edit" | "SystemEvent";
    type ArchiveSearchType = "DEFAULT" | "COMMENT" | "CUSTOM_FIELD";
    type CommentSource =
      | "REGULAR COMMENT"
      | "APPROVAL COMMENT"
      | "REJECTION COMMENT"
      | "REASSIGNMENT COMMENT"
      | "EDIT COMMENT"
      | "CLONE COMMENT";
    type BudgetType =
      | "IN_BUDGET"
      | "OUT_OF_BUDGET"
      | "REPLACEMENT"
      | "TEMPORARY_REPLACEMENT"
      | "REOPENING";
    type LDAPAttribute = keyof LDAPPerson;

    type HotelPaymentType =
      | "Guest to pay at the reception"
      | "MAX IV payment"
      | "Online payment";

    type EmailType =
      | "SubmissionConfirmation"
      | "SubmissionNotification"
      | "ResubmissionConfirmation"
      | "ResubmissionNotification"
      | "DecisionNotificationSubmitter";
    interface DefaultData {
      forms: {
        [key in System.FormType]: {
          projects: Project[];
          supervisors: Supervisor[];
        };
      };
    }

    interface JWT {
      name: string;
      username: string;
      email: string;
      groups: string[];
    }

    interface LDAPPerson {
      fullName: string;
      firstName: string;
      title: string;
      office: string;
      phone: string;
      memberOf: string[];
      username: string;
      email: string;
      jpeg: string;
      DN: string;
    }
    interface LDAPPersonUpdate {
      fullName: string;
      title: string;
      office: string;
      phone: string;
      email: string;
      DN: string;
    }
    interface Access {
      name: string;
      username: string;
      email: string;
      groups: string[];
      authenticated: boolean;
      admin: {
        [key in System.FormType]: boolean;
      };
    }

    interface APIError {
      msg?: string;
      code: number;
    }

    type DbType = "TEST" | "DEVELOPMENT" | "PRODUCTION";
    interface DBData {
      type: DbType;
    }

    interface User {
      email: string;
      name: string;
      phone: string;
      username: string;
      groups: string[];
      admin: {
        [key in System.FormType]: boolean;
      };
    }

    type FormReadAccess = "NONE" | "MINIMAL" | "BASIC" | "DEFAULT" | "FULL";
    interface FormAccess {
      readAccess: FormReadAccess;
      canEdit: boolean;
      canResolve: boolean;
      canDelete: boolean;
      isStaff: boolean;
      isSubmitter: boolean;
      isSupervisor: boolean;
      isAdmin: boolean;
      form?: System.Form; //Included to prevent additional db read. Always defined if read === true
    }
    interface FormConfigs {
      forms: {
        [key in System.FormType]: System.FormConfig;
      };
    }
    interface FormConfig {
      adminGroups: string[];
      adminUsers: string[];
      path: string;
      formType: FormType;
      name: string;
      requiresApproval: boolean;
      canEditAdmin: boolean;
      canEditSupervisor: boolean;
      approvedFormsPublic: boolean;
      hideApprovedForms: boolean;
      model: mongoose.Model<any>;
      /**called after initial submit on the form. Return false to prevent default email notification behaviour
       * NOTE: not called on a resubmittal! A resubmittal is identified as an edit, with formUpdate.updateType === "RESUBMITTAL".
       */
      onSubmit?: (form: System.Form) => boolean;
      /**Called after form edit or resubmittal. Return false to prevent default email notification behaviour */
      onEdit?: (
        formUpdate: System.FormUpdate,
        access: System.Access
      ) => boolean;
      /**Called after form supervisor reassignment. Return false to prevent default email notification behaviour */
      onReassign?: (
        formUpdate: System.FormUpdate,
        access: System.Access
      ) => boolean;
      /**Called after form finalization (rejection or non-reassigning approval). Return false to prevent default email notification behaviour */
      onDecision?: (
        formUpdate: System.FormUpdate,
        access: System.Access
      ) => boolean;
      /**Called after addition of comment to form. Return false to prevent default email notification behaviour */
      onComment?: (
        formUpdate: System.FormUpdate,
        access: System.Access
      ) => boolean;
      additionalTexts: {
        SubmissionConfirmation?: (form: System.Form) => string;
        SubmissionNotification?: (form: System.Form) => string;
        ResubmissionConfirmation?: (form: System.Form) => string;
        ResubmissionNotification?: (form: System.Form) => string;
        DecisionNotificationSubmitter?: (form: System.Form) => string;
      };
    }

    /**
     * Base interface used to provide context (values for placeholders) for Handlebars email template.
     */
    interface EmailContext {
      domainName: string;
      subject: string;
    }
    interface SubmissionNotification extends EmailContext {
      submissionNotification: {
        formName: string;
        submitterName: string;
        formId: string;
        link: string;
        additionalMsg: string | undefined;
      };
    }
    interface SubmissionConfirmation extends EmailContext {
      submissionConfirmation: {
        formName: string;
        supervisorName: string;
        requiresApproval: boolean;
        additionalMsg: string;
      };
    }
    interface ResubmissionNotification extends EmailContext {
      resubmissionNotification: {
        formName: string;
        submitterName: string;
        formId: string;
        link: string;
        additionalMsg: string | undefined;
      };
    }
    interface ResubmissionConfirmation extends EmailContext {
      resubmissionConfirmation: {
        formName: string;
        supervisorName: string;
        additionalMsg: string;
      };
    }
    interface EditNotification extends EmailContext {
      editNotification: {
        formName: string;
        editorName: string;
        submitterName: string;
        formId: string;
        link: string;
      };
    }
    interface CommentNotification extends EmailContext {
      commentNotification: {
        formName: string;
        commentatorName: string;
        submitterName: string;
        formId: string;
        link: string;
      };
    }
    interface ReassignmentNotification extends EmailContext {
      reassignmentNotification: {
        formName: string;
        reassignerName: string;
        submitterName: string;
        formId: string;
        link: string;
        hasComment: boolean;
        commentTitle: string;
        commentMsg: string;
      };
    }
    interface DecisionNotificationListeners extends EmailContext {
      decisionNotificationListeners: {
        formName: string;
        formStatus: string;
        submitterName: string;
        deciderName: string;
      };
    }
    interface DecisionNotificationSubmitter extends EmailContext {
      decisionNotificationSubmitter: {
        formName: string;
        formId: string;
        formStatus: string;
        deciderName: string;
        hasComment: boolean;
        commentTitle: string;
        commentMsg: string;
        additionalMsg: string;
        isRejection: boolean;
        link: string;
      };
    }
    interface SupervisorReminder extends EmailContext {
      supervisorReminder: {
        formName: string;
        submitterName: string;
        formId: string;
        link: string;
        profileViewLink: string;
      };
    }

    interface SafetyTestReminder extends EmailContext {
      safetyTestReminder: {
        name: string;
        timeSv: string;
        timeEn: string;
        link: string;
      };
    }
    interface SimpleFormNotification extends EmailContext {
      simpleForm: SimpleForm;
    }
    interface GeneralNotification extends EmailContext {
      generalNotification: {
        messages: string[];
      }
    }
    interface withId {
      _id: string;
    }
    //MODELS
    interface Counter {
      _id?: string;
      sequenceValue: number;
    }
    interface DBConfig {
      ldapCacheTimestamp: Date;
    }
    interface Collection {
      name?: string;
      form: System.FormType;
    }
    interface Supervisor extends Collection {
      projects: string[];
      email: string;
    }

    interface ListItem {
      label: string;
      value: string;
      _id?: string;
    }
    interface List extends Collection {
      items: ListItem[];
      defaultValue: string;
      description: string;
    }

    interface Project extends Collection {
      project: string;
      category: string;
    }

    interface LDAPRequest {
      searchQuery: string;
      attribute: System.LDAPAttribute;
    }
    interface FormUpdate {
      form: System.Form;
      updateType: FormUpdateType;
      disableNotify?: boolean; //if true, supervisors will not be notified even if they checked "notify me...". Currently only used for comments
      comment?: Comment;
      edit?: Edit;
      //systemEvent or supervisorAction are not needed (yet)
    }

    interface Event {
      _id?: string;
      eventType: EventType;
      timestamp: Date;
      role?: EventRole;
    }

    interface SystemEvent extends Event {
      action: string;
      agent: string;
      description: string;
      color: string;
    }
    interface SupervisorAction extends Event {
      action: FormStatusAction;
      decisionDate: Date | undefined;
      name: string;
      email: string;
      comment: string;
      notify: boolean;
    }
    interface Edit extends Event {
      name: string;
      email: string;
    }

    interface Comment extends Event {
      updated: Date;
      source: CommentSource;
      msg: string;
      author: string;
      status: FormStatus;
      supervisor: string;
    }
    type ScheduledEventType = "SAFETY_TEST_REMINDER";

    interface SafetyTestReminderEvent extends ScheduledEvent {
      type: "SAFETY_TEST_REMINDER";
      payload: { email: string; name: string; reminderCount: number };
    }

    interface ScheduledEvent {
      type: ScheduledEventType;
      executionTime: Date;
      payload: any;
    }

    interface SimpleForm {
      formType: string;
      emailSubject: string;
      emailRecipient: string;
      data: any;
    }

    /**
     * Base interface for all non-simle forms
     */
    interface Form {
      _id?: string;
      externalId?: string;
      formType: System.FormType;
      submitterName: string;
      submitterEmail: string;
      submissionDate: Date;
      files: Array<{ name: string; size: string }>;
      status: System.FormStatus;
      edits?: Edit[];
      comments?: Comment[];
      supervisorActions: SupervisorAction[];
      systemEvents: SystemEvent[];
      toObject(options?: DocumentToObjectOptions): any;
      postponedUntil?: Date | undefined | null;
      isArchived: boolean;
    }

    interface HotelForm extends Form {
      submitterPhoneNumber: string;
      guestName: string;
      guestEmail: string;
      guestPhoneNumber: string;
      checkInDate: Date | undefined;
      checkOutDate: Date | undefined;
      lateArrival: boolean;
      breakfast: boolean;
      paymentType: HotelPaymentType;
      activityNumber: string;
      additionalMsg: string;
    }
    interface TravelForm extends Form {
      project: string;
      destination: string;
      travelPurpose: string;
      startDate: Date;
      endDate: Date;
      expenses: {
        travel: number;
        accommodation: number;
        conferenceFee: number;
        subsistence: number;
      };
      totalExpenses: number;
    }
    interface PurchaseForm extends Form {
      project: string;
      motivation: string;
      bidder1: string;
      bidder2: string;
      bidder3: string;
      supplierName: string;
      frameworkAgrmt: boolean;
      description: string;
      environment: string;
      product: string;
      comment: string;
      amount: number;
    }
    interface EmploymentRequest extends Form {
      position: string;
      group: string;
      team: string;
      employmentType: "Permanent" | "Temporary" | "";
      extent: number;
      estStartDate: Date;
      estEndDate: Date | undefined;
      estMonthlySalary: number;
      contactedOfficeCoordinator: boolean;
      rejectionAction: string;
      budgetType: BudgetType;
      budgetTypeDetail: string;
      budgetTypeDetail2: string;
      externallyFundedProject: string;
      externallyFundedPercentage: number;
      primulaGroup: string;
    }

    interface GrantInitiativeForm extends Form {
      responsible: string;
      coordinator: string;
      otherParties: string;
      collaborators: string;
      fundingAgency: string;
      projectName: string;
      projectDescription: string;
      beamlineAccessType: string;
      officeSpaceCount: number;
      pooledResource: string;
      ownership: "Yes" | "No" | "N/A";
      otherResources: string;
      strategyAlignment: string;
      startingYear: number;
      people: {
        firstName: string;
        lastName: string;
        inKind: boolean;
        years: { salaryPercentage: number; nbrMonths: number }[];
      }[];
      funding: {
        salaries: number[];
        investments: number[];
        travel: number[];
        others: number[];
        indirectCost: number[];
      };
      cofunding: {
        salaries: number[];
        others: number[];
        indirectCost: number[];
      };
    }
    interface AdditionalFundingForm extends Form {
      responsibleDirector: string;
      applicant: string;
      organizationalUnitType: "Beamline" | "Group";
      organizationalUnitName: string;
      totalAmountRequested: number;
      investmentDescription: string;
      investmentBackground: string;
      contingencyAmountRemaining: number;
      totalAmountApproved: number;
      fundingSource:
        | ""
        | "Upkeep"
        | "DM Ex contingency"
        | "DM Ex contingency (out of budget)";
    }
    interface CateringRequest extends Form {
      deliveryPoint: string;
      deliveryDate: Date | undefined;
      nameOfTheMeeting: string; // No longer used, expect for old forms
      purpose: string;
      activity: string;
      orders: { deliveryTime: Date; servingType: string; quantity: number }[];
      otherRequests: string;
      participants: string;
    }
    interface ChemicalSample {
      name: string;
      cas: string;
      quantity: string;
      aggregationState:
        | "Solid/Powder"
        | "Solid/Piece"
        | "Single"
        | "Liquid"
        | "Gas";
      safetyClassifications: string[];
    }
    interface NanoSample {
      name: string;
      source: "Commercial" | "Natural" | "Other";
      sourceDetail: string;
      aggregationState:
        | "Powder"
        | "Aerosol"
        | "In Suspension/Liquid"
        | "On Solid Matrix";
      quantity: string;
      sizeDistribution: string;
      length: string;
      density: string;
      class: 0 | 1 | 2 | 3;
    }
    interface BioSample {
      category:
        | "Proteins"
        | "Lipids"
        | "Antibodies"
        | "Bacteria"
        | "Viral components"
        | "Mammalian cells"
        | "Human or animal tissue"
        | "Other";
      name: string;
      source: "Commercial" | "Laboratory made" | "Animal";
      sourceDetail: string;
      aggregationState: string;
      quantity: string;
    }
    interface RiskEquipment {
      mitigations: string;
      checks: string[];
    }
    type RiskType =
      | "vessles"
      | "heater"
      | "cryostat"
      | "magnetic"
      | "electrochemical"
      | "laser"
      | "lamps"
      | "sonic"
      | "blowtorch"
      | "ribbon";

    interface ExperimentDetail {
      details: string;
      risks: string;
      mitigation: string;
      emergencyAction: string;
    }
    interface RiskAssessmentForm extends Form {
      proposalId: string;
      title: string;
      safetyClassification: string;
      hasDates: boolean;
      startDate?: Date;
      endDate?: Date;
      beamline: string;
      contactName: string;
      contactEmail: string;
      contactPhone: string;
      proposerName: string;
      proposerEmail: string;
      proposerPhone: string;
      PIName: string;
      PIEmail: string;
      PIPhone: string;
      companyName: string;
      significantModifications: string;
      sampleRemoved: "MAX IV" | "User";
      labAccessRequired: boolean;
      customBuiltEquipmentDescription: string;
      additionalChemicalProperties: string;
      additionalNanoProperties: string;
      equipments: {
        [key in RiskType]: RiskEquipment;
      };
      chemicalSamples: ChemicalSample[];
      nanoSamples: NanoSample[];
      bioSamples: BioSample[];
      experimentDetails: {
        lab: ExperimentDetail;
        beamline: ExperimentDetail;
        other: ExperimentDetail;
      };
      hasSignificantModifications: boolean;
      hasHazardousEquipment: boolean;
      hasOwnEquipment: boolean;
      hasChemicalSamples: boolean;
      hasNanoSamples: boolean;
      hasBioSamples: boolean;
      hasCMRCompound: boolean;
    }
    type SafetyTestAnswerCategory = "BASIC" | "RADIATION" | "CHEMICAL" | "BIO";
    interface SafetyTestFormAnswer {
      category: SafetyTestAnswerCategory;
      qId: string;
      aId: string;
    }
    interface SafetyTestForm extends Form {
      employer: string;
      contactPerson: string;
      moreThanTwoMonths: boolean;
      phoneNumber: string;
      signature: boolean;
      answers: SafetyTestFormAnswer[];
      hasRadiation: boolean;
      hasChemical: boolean;
      hasBio: boolean;
    }
  }
}
