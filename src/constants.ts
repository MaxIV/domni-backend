export const PURCHASE: System.FormType = "PURCHASE_APPLICATION";
export const TRAVEL: System.FormType = "TRAVEL";
export const EMPLOYMENT: System.FormType = "EMPLOYMENT_REQUEST";
export const GRANT_INITIATIVE: System.FormType = "GRANT_INITIATIVE";
export const ADDITIONAL_FUNDING: System.FormType = "ADDITIONAL_FUNDING";
export const HOTEL: System.FormType = "HOTEL_BOOKING";
export const CATERING: System.FormType = "CATERING_REQUEST";
export const RISK_ASSESSMENT: System.FormType = "RISK_ASSESSMENT";
export const SAFETY_TEST: System.FormType = "SAFETY_TEST";

export const AD_GROUP_EST = "EST";
export const AD_GROUP_FINANCE = "Ekonomi";
export const AD_GROUP_ADMIN = "Administration";
export const AD_GROUP_IMS = "Information Management";
export const AD_GROUP_AD_ADMIN = "account-admins";
export const AD_GROUP_RADIATION_SAFETY = "Rad";
