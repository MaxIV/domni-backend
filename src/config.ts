import * as Constants from "./constants";
import {
  AdditionalFundingForm,
  CateringRequest,
  EmploymentRequest,
  GrantInitiativeForm,
  HotelForm,
  PurchaseForm,
  TravelForm,
  RiskAssessment,
  SafetyTestForm,
} from "./models/mongoSchema";
import {
  addSystemEvent,
  createSafetyTestReminders,
  getLatestSupervisorAction,
  getOldestSupervisorAction,
} from "./utilities/Helpers";
import * as mailer from "./utilities/mailer";
import { sendMailMaxIV } from "./utilities/mailer";

export const SENDER_EMAIL = "domni@maxiv.lu.se";
export const formConfigs: System.FormConfigs = {
  forms: {
    RISK_ASSESSMENT: {
      adminUsers: [],
      adminGroups: [Constants.AD_GROUP_EST],
      path: "risk-assessment",
      formType: Constants.RISK_ASSESSMENT,
      name: "Risk Assessment",
      requiresApproval: true,
      canEditAdmin: true,
      canEditSupervisor: false,
      approvedFormsPublic: true,
      hideApprovedForms: false,
      onEdit: (formUpdate, access) => {
        const form = formUpdate.form as System.RiskAssessmentForm;
        // set week and external id
        form.externalId = form.proposalId;
        const newF = RiskAssessment.findOneAndUpdate(
          { _id: form._id },
          form,
          (err, doc, res: any) => {
            if (err) {
              console.log("Failed to set proposal id as external id");
            }
          }
        );
        return true;
      },
      onSubmit: (form: System.RiskAssessmentForm) => {
        // set week and external id
        form.externalId = form.proposalId;
        const newF = RiskAssessment.findOneAndUpdate(
          { _id: form._id },
          form,
          (err, doc, res: any) => {
            if (err) {
              console.log("Failed to set proposal id as external id");
            }
          }
        );
        const laser = form.equipments.laser;
        if (laser.checks.length > 0) {
          // If there is going to be a laser, then laser safety needs to know.
          const emailContext: System.GeneralNotification = {
            domainName: process.env.DOMAIN_NAME,
            subject: `ESRA ${form.proposalId} includes a laser.`,
            generalNotification: {
              messages: [
                `Laser-laden ESRA submitted by ${form.proposerName} (${form.proposerEmail})`,
                `Selected hazards: ${form.equipments.laser.checks.join(', ')}`,
                `Mitigations: ${laser.mitigations}`,
                `Self brought equipment: ${form.customBuiltEquipmentDescription || '[none]'}`
              ]
            },
          };
          sendMailMaxIV(SENDER_EMAIL, "domenico.alj@maxiv.lu.se", emailContext);
        }

        return true;
      },
      model: RiskAssessment,
      additionalTexts: {
        DecisionNotificationSubmitter: (form: System.Form) => {
          const approvedMessage =
            form.status === "APPROVED"
              ? "In case you need to do any change to your Approved Risk Assessment, contact est@maxiv.lu.se to unlock it. You can still upload additional files via your profile."
              : "";
          return `This is the risk assessment for DUO proposal ${form.externalId}. ${approvedMessage}`;
        },
        SubmissionConfirmation: (form: System.Form) => {
          return `This is the risk assessment for DUO proposal ${form.externalId}`;
        },
        SubmissionNotification: (form: System.Form) => {
          return `This is the risk assessment for DUO proposal ${form.externalId}`;
        },
      },
    },
    PURCHASE_APPLICATION: {
      adminUsers: [],
      adminGroups: [Constants.AD_GROUP_FINANCE],
      path: "purchase-application",
      formType: Constants.PURCHASE,
      name: "Purchase Application",
      requiresApproval: true,
      canEditAdmin: true,
      canEditSupervisor: false,
      approvedFormsPublic: false,
      hideApprovedForms: false,
      model: PurchaseForm,
      // purchase app has special actions upon final approval: email "registrator" and also log this
      onDecision: (formUpdate, access) => {
        if (formUpdate.form.status === "APPROVED") {
          mailer.simpleForm(
            "New purchase application approved in Domni",
            "registrator@maxiv.lu.se",
            formUpdate.form
          );
          addSystemEvent(
            formUpdate.form,
            {
              action: "External Reg.",
              agent: "Domni",
              role: "System",
              description:
                'Registration of the approval in the central database (sv: "diarieföring").',
              color: "black",
            },
            10000
          );
        }
        return true;
      },
      additionalTexts: {},
    },
    TRAVEL: {
      adminUsers: [],
      adminGroups: [Constants.AD_GROUP_FINANCE],
      path: "travel-approval",
      formType: Constants.TRAVEL,
      name: "Travel Approval",
      requiresApproval: true,
      canEditAdmin: false,
      canEditSupervisor: false,
      approvedFormsPublic: false,
      hideApprovedForms: false,
      model: TravelForm,
      additionalTexts: {},
    },
    EMPLOYMENT_REQUEST: {
      adminUsers: ["lenqvi"],
      adminGroups: [Constants.AD_GROUP_FINANCE],
      path: "employment-request",
      formType: Constants.EMPLOYMENT,
      name: "Employment Request",
      requiresApproval: true,
      canEditAdmin: true,
      canEditSupervisor: false,
      approvedFormsPublic: false,
      hideApprovedForms: false,
      model: EmploymentRequest,
      onDecision: (formUpdate, access) => {
        if (formUpdate.form.status === "APPROVED") {
          const emailContext: System.DecisionNotificationListeners = {
            domainName: process.env.DOMAIN_NAME,
            subject: `Approval: Employment Request  #${formUpdate.form._id}`,
            decisionNotificationListeners: {
              formName: "Employment Request",
              submitterName: formUpdate.form.submitterName,
              formStatus: formUpdate.form.status,
              deciderName: access.name,
            },
          };
          sendMailMaxIV(
            SENDER_EMAIL,
            "ann.sjunnesson@maxiv.lu.se",
            emailContext
          );

          addSystemEvent(
            formUpdate.form,
            {
              action: "Email notification.",
              agent: "Domni",
              role: "System",
              description:
                "Notifying Ann Sjunnesson about the approval via email.",
              color: "black",
            },
            10000
          );
        }
        return true;
      },
      additionalTexts: {},
    },
    GRANT_INITIATIVE: {
      adminUsers: ["dororb"],
      adminGroups: [Constants.AD_GROUP_FINANCE],
      path: "grant-initiative",
      formType: Constants.GRANT_INITIATIVE,
      name: "Grant Initiative",
      requiresApproval: true,
      canEditAdmin: true,
      canEditSupervisor: false,
      approvedFormsPublic: false,
      hideApprovedForms: false,
      model: GrantInitiativeForm,
      // grant initiative has special actions upon final approval: email "registrator" and also log this
      onDecision: (formUpdate, access) => {
        if (formUpdate.form.status === "APPROVED") {
          mailer.simpleForm(
            "New Grant Initiative Form approved in Domni",
            "registrator@maxiv.lu.se",
            formUpdate.form
          );
          addSystemEvent(
            formUpdate.form,
            {
              action: "External Reg.",
              agent: "Domni",
              role: "System",
              description:
                "Registration of the approval in the central database.",
              color: "black",
            },
            10000
          );
        }
        return true;
      },
      additionalTexts: {},
    },
    ADDITIONAL_FUNDING: {
      adminUsers: ["lenqvi"],
      adminGroups: [Constants.AD_GROUP_FINANCE],
      path: "additional-funding",
      formType: Constants.ADDITIONAL_FUNDING,
      name: "Additional Funding Request",
      requiresApproval: true,
      canEditAdmin: true,
      canEditSupervisor: true,
      approvedFormsPublic: false,
      hideApprovedForms: false,
      model: AdditionalFundingForm,
      // additional actions on final decision: email finance team to inform them of the approval/rejection
      onDecision: (formUpdate, access) => {
        const subjectPrefix =
          formUpdate.form.status === "APPROVED" ? "Approval" : "Rejected";
        const emailContext: System.DecisionNotificationListeners = {
          domainName: process.env.DOMAIN_NAME,
          subject: `${subjectPrefix}: Additional Funding Request #${formUpdate.form._id}`,
          decisionNotificationListeners: {
            formName: "Additional Funding Request",
            submitterName: formUpdate.form.submitterName,
            formStatus: formUpdate.form.status,
            deciderName: access.name,
          },
        };
        sendMailMaxIV(SENDER_EMAIL, "finance@maxiv.lu.se", emailContext);
        addSystemEvent(
          formUpdate.form,
          {
            action: "Notified",
            agent: "Domni",
            role: "System",
            description:
              "The Finance team has been notified of this decision by email",
            color: "black",
          },
          1000
        );
        return true;
      },
      additionalTexts: {
        DecisionNotificationSubmitter: (_) => {
          return "When the invoice arrives, please contact Finance for accounting information for the correct budget.";
        },
      },
    },
    HOTEL_BOOKING: {
      adminUsers: [],
      adminGroups: [Constants.AD_GROUP_ADMIN],
      path: "hotel-booking",
      formType: Constants.HOTEL,
      name: "Hotel Booking",
      requiresApproval: false,
      canEditAdmin: false,
      canEditSupervisor: false,
      approvedFormsPublic: false,
      hideApprovedForms: false,
      model: HotelForm,
      // Don't notify the submitter that their booking has been approved (requested by client)
      onDecision: (formUpdate, access) => false,
      additionalTexts: {},
    },
    CATERING_REQUEST: {
      adminUsers: [],
      adminGroups: [Constants.AD_GROUP_ADMIN],
      path: "catering-request",
      formType: Constants.CATERING,
      name: "Catering Request",
      requiresApproval: false,
      canEditAdmin: false,
      canEditSupervisor: false,
      approvedFormsPublic: false,
      hideApprovedForms: false,
      model: CateringRequest,
      additionalTexts: {
        DecisionNotificationSubmitter: (form) =>
          `<p>Your order is now placed in Lupin where the responsible GM or project manager need to approve it.
          When the order is approved you will receive an email from Lupin. </p>
          <p><b>IMPORTANT!</b> When your meeting is over you need to log in to Lupin and confirm the order and add the participants of the meeting as a comment.</p>`,
      },
    },
    SAFETY_TEST: {
      adminUsers: ["isalin", "andros"],
      adminGroups: [Constants.AD_GROUP_ADMIN, Constants.AD_GROUP_RADIATION_SAFETY],
      path: "safety-test",
      formType: Constants.SAFETY_TEST,
      name: "Safety Test",
      requiresApproval: false,
      canEditAdmin: false,
      canEditSupervisor: false,
      approvedFormsPublic: false,
      hideApprovedForms: true,
      model: SafetyTestForm,
      onDecision: (formUpdate, access) => {
        if (formUpdate.form.status === "APPROVED") {
          createSafetyTestReminders(formUpdate);
        }
        return true;
      },
      additionalTexts: {},
    },
  },
};

export const getFormConfigs = (): System.FormConfig[] => {
  return getFormTypes().map((key) => formConfigs.forms[key]);
};
export const getFormTypes = (): System.FormType[] => {
  return Object.keys(formConfigs.forms) as System.FormType[];
};
export const getFormConfig = (formType: System.FormType): System.FormConfig => {
  return formConfigs.forms[formType];
};

export const getFormConfigByPath = (
  matchParams: string[]
): System.FormConfig => {
  return getFormConfig(getFormTypeByPath(matchParams));
};

export const getFormTypeByPath = (matchParams: string[]): System.FormType => {
  const path = matchParams[0];
  let formType;
  Object.keys(formConfigs.forms).forEach((form) => {
    if (formConfigs.forms[form as System.FormType].path === path) {
      formType = form;
    }
  });
  if (!formType) {
    console.log("Unknown form type for path " + path);
    throw new Error("Unknown form type for path " + path);
  }
  return formType;
};
