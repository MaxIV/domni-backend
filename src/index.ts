import app from "./app";
import http from "http";
import fs from "fs";
import mongoose from "mongoose";
import { initMissingDefaultData } from "./defaultData";
import { deleteAllForms, clearCounter } from "./database/clear";

const dotenv = require("dotenv");
dotenv.config();

const filesDir = "./files";
const backupDir = "./files/backup";

if (!fs.existsSync(filesDir)) {
  fs.mkdirSync(filesDir);
}
if (!fs.existsSync(backupDir)) {
  fs.mkdirSync(backupDir);
}
const port = parseInt(process.env.PORT || "4000", 10);
app.set("port", port);

const server = http.createServer(app);

server.listen(port);
server.on("error", onError);
server.on("listening", onListening);

/**
 * Event listener for HTTP server "error" event.
 */

function onError(error: any) {
  if (error.syscall !== "listen") {
    throw error;
  }

  const bind = typeof port === "string" ? "Pipe " + port : "Port " + port;

  // handle specific listen errors with friendly messages
  switch (error.code) {
    case "EACCES":
      console.error(bind + " requires elevated privileges");
      process.exit(1);
    case "EADDRINUSE":
      console.error(bind + " is already in use");
      process.exit(1);
    default:
      throw error;
  }
}
async function onListening() {
  try {
    const db = mongoose.connection;
    db.on("connecting", function () {
      console.log("Connecting to mongodb at " + process.env.MONGO);
    });
    db.on("connected", function () {
      console.log("Connected to mongodb. Server running on port " + port);
    });
    db.on("error", function (error) {
      console.error("Error in MongoDb connection: " + error);
    });
    db.on("reconnected", function () {
      console.log("MongoDB reconnected!");
    });
    db.on("disconnected", function () {
      console.log(
        "MongoDB disconnected. Will attempt to reconnect every 10 seconds"
      );
    });
    await mongoose.connect(process.env.MONGO, {
      useNewUrlParser: true,
      useFindAndModify: false,
      autoReconnect: true,
      reconnectTries: (3600 / 10) * 24 * 3, // reconnects over a whole weekend
      reconnectInterval: 1000 * 10, // reconnect every 10 seconds
    });
    await initMissingDefaultData();
  } catch (err) {
    console.log("failed connecting to mongodb:");
    console.log(err);
  }
}
export const dumpCollections = async () => {
  console.log(`Creating backups in ${backupDir}`);
  mongoose.modelNames().forEach(async (modelName) => {
    const documents = await mongoose.model(modelName).find();
    const data: any[] = [];
    documents.map((doc) => data.push(doc.toObject()));
    const month = new Date().getMonth();
    fs.writeFile(
      `${backupDir}/${modelName}-${month}.json`,
      JSON.stringify(data),
      (err) => {
        console.log(`Backed up ${modelName}`);
      }
    );
  });
};
